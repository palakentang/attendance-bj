<?php

use Carbon\Carbon;

return [
    'error' => [
        'absent_has_been_out' => 'Anda Sudah Absen Pulang',
        'absent_not_time_to_go' => 'Anda Belum {many_hours_of_work} Jam Bekerja',
    ],
    'path_upload' => 'uploads/absensi/'.date('Y-m-d').'/',
    'late_time_in' => '09:30',
    'late_time_out' => '22:00',
    'limit_late_time_in' => '2400'

];
