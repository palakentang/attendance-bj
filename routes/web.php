<?php

use App\Http\Controllers\SettingsController;
use App\Models\User;
use App\Scopes\YanipScope;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Middleware\TenantMiddleware;
use App\Http\Controllers\TenantsController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\AttendancesController;
use App\Http\Controllers\auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('auth/login/index');
});


Route::namespace('Auth')->name('auth.')->prefix('auth/')->group(function ()
{
    Route::name('login.')->prefix('login/')->group(function ()
    {
        Route::get('index',[LoginController::class, 'index'])->name('index');
        Route::post('process',[LoginController::class, 'store'])->name('store');
    });
    Route::name('register.')->prefix('register/')->group(function ()
    {
        Route::get('/index', [RegisterController::class, 'index'])->name('index');
        Route::get('/proccess', [RegisterController::class, 'store'])->name('store');
    });
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
});

/**
 * Admin Area
 */
Route::group(['middleware' => ['role:superadmin|staff|admin', 'auth']], function () {

    Route::name('admin.')->prefix('/admin')->group(function ()
    {
        // Route::get('attendances/export')
        Route::post('attendances/lists', 'Admin\AttendancesController@lists')->name('attendances.lists');
        Route::post('attendances/lists/filter', 'Admin\AttendancesController@show')->name('attendances.lists.filter');
        Route::post('paid-leaves/lists', 'Admin\PaidLeaveController@lists')->name('paid-leaves.lists');
        Route::post('dinas/lists', 'Admin\DinasController@lists')->name('dinas.lists');

        Route::get('paid-leaves/show/{cat_id}/{user_id}', 'Admin\PaidLeaveController@showByCatIDnUserID')->name('paid-leaves.showByCatIDnUserID');
        Route::get('paid-leaves/users', 'Admin\PaidLeaveController@lists_users')->name('paid-leaves.users.lists');
        Route::resource('paid-leaves', 'Admin\PaidLeaveController');
        Route::resource('sick-leaves', 'Admin\SickLeaveController');

        Route::resource('attendances', 'Admin\AttendancesController');
        Route::resource('dinas', 'Admin\DinasController');

        Route::get('/clear-cahce', function (){Artisan::call('cache:clear');});
        Route::post('update-password', 'UserController@updatePassword')->name('update-password');
    });

});

Route::group(['middleware' => ['auth']], function () {
    Route::get('dashboard', 'Admin\HomeController@index')->name('staff.dashboard');
    Route::get('dashboard/show-detail/{user_id}/{module}', 'Admin\HomeController@show')->name('show.detail.module');
    Route::post('notifications/store', 'NotificationController@store')->name('send.notification.store');
});


// Superadmin
Route::group(['middleware' => ['role:superadmin|staff', 'auth']], function () {

    Route::post('import', 'Admin\AttendancesController@import')->name('attendance.import');
    Route::get('export', 'Admin\AttendancesController@export')->name('attendance.export');

    Route::resource('users','Admin\UserController');
    Route::post('users', 'Admin\UserController@import')->name('users.import');
    Route::post('users/store', 'Admin\UserController@store')->name('users.store');
    Route::resource('roles','Admin\RoleController');
    Route::resource('permissions','Admin\PermissionController');

    Route::post('paid-leaves/print', 'PaidLeavesController@print')->name('paid-leave.print');
    Route::resource('paid-leaves','PaidLeavesController');
    Route::resource('dinas','DinasController');

    Route::post('lists/users', 'Admin\HomeController@lists')->name('admin.dashboard.lists');

    Route::resource('settings', 'SettingsController');
});

// PJLP
Route::group(['middleware' => ['role:pjlp|staff', 'auth']], function () {
    Route::post('attendances/lists', 'AttendancesController@lists')->name('attendances.lists');
    Route::get('dashboard-pjlp', 'HomeController@index')->name('dashboard');
    Route::post('attendance', 'AttendancesController@store')->name('attendance.store');
    Route::get('attendance', 'AttendancesController@index')->name('attendance.index');
    Route::resource('dinas','DinasController');
    Route::post('paid-leaves/sick', 'PaidLeavesController@sick')->name('paid-leave.sick');
    Route::post('paid-leaves/print', 'PaidLeavesController@print')->name('paid-leave.print');
    Route::resource('paid-leaves','PaidLeavesController');
    Route::post('update-password', 'UserController@updatePassword')->name('update-password');
});

// Admin
Route::group(['middleware' => ['role:admin', 'auth']], function () {

});

Route::group(['middleware' => ['auth']], function () {
    Route::post('auth/sip', 'Auth\LoginController@loginFromOtherSite');
});

