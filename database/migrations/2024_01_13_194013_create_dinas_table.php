<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_services', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedBigInteger('service_cat_id');
            $table->foreign('service_cat_id')
                    ->references('id')
                    ->on('tb_service_cat')
                    ->onDelete('cascade');
            $table->string('name');
            $table->string('service_number');
            $table->string('service_description')->nullable();
            $table->date('service_date_start');
            $table->date('service_date_end');
            $table->unsignedBigInteger('commissioned_by');
            $table->foreign('commissioned_by')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_services');
    }
}
