<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldVerifiedToTbPaidLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_paid_leave', function (Blueprint $table) {
            $table->unsignedBigInteger('verified_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_paid_leave', function (Blueprint $table) {
            $table->dropColumn('verified_by');
        });
    }
}
