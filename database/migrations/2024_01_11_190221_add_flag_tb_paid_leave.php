<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlagTbPaidLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_paid_leave', function (Blueprint $table) {
            $table->enum('flag', ['submit', 'approved', 'declined'])->default('submit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_paid_leave', function (Blueprint $table) {
            $table->dropColumn('flag');
        });
    }
}
