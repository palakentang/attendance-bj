<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTbServiceDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_service_detail', function (Blueprint $table) {
            $table->id();
            $table->uuid('service_id')->nullable(false);
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                 ->on('users')
                 ->references('id')
                 ->onDelete('cascade');
            $table->text('letter_of_assignment')->nullable();
     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_service_detail');
    }
}
