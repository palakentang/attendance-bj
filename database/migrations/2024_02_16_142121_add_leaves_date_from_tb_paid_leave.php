<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLeavesDateFromTbPaidLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_paid_leave', function (Blueprint $table) {
            $table->string('list_date_paid_leave')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_paid_leave', function (Blueprint $table) {
            $table->dropColumn('list_date_paid_leave');
        });
    }
}
