<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFlagToTbServiceDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_service_detail', function (Blueprint $table) {
            $table->enum('flag', ['submit', 'approved'])->default('submit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_service_detail', function (Blueprint $table) {
            $table->dropColumn('flag');
        });
    }
}
