<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Tenants;
use Illuminate\Database\Seeder;

class TenantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tenants::insert([
            [
                'tenant_name' => 'yanip',
                'flag' => 'enabled',
                'url' => \Str::slug('yanip')
            ],
            [
                'tenant_name' => 'berita jakarta',
                'flag' => 'enabled',
                'url' => \Str::slug('berita jakarta')
            ]
        ]);
    }
}
