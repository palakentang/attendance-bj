<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',

            // Absensi
            'attendance-list',
            'attendance-create',
            'attendance-edit',
            'attendance-delete',

            // Cuti
            'leave-list',
            'leave-create',
            'leave-edit',
            'leave-delete',

            // Dinas
            'dinas-list',
            'dinas-create',
            'dinas-edit',
            'dinas-delete',

            // User
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',

            // Permission
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
         ];

         foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
    }
}
