<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        $this->call(PermissionTableSeeder::class);
        $this->call(TenantsSeeder::class);
        $this->call(UsersAdminSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PaidLeaveCatSeeder::class);
        $this->call(PaidLeaveSeeder::class);

        $this->call(ServiceCatSeeder::class);
    }
}
