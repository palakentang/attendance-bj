<?php

namespace Database\Seeders;

use App\Models\PaidLeave;
use Illuminate\Database\Seeder;

class PaidLeaveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paid_leave = [
            [
                'id' => \Str::uuid()->toString(),
                'user_id' => 4,
                'category_leave_id' => 1,
                'days' => 3,
                'leave_date' => '2024-01-15',
                'explaination' => 'Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle quora plaxo ideeli hulu weebly balihoo...',
                'evidence' => null,
                'flag' => 'approved',
                'verified_by' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => \Str::uuid()->toString(),
                'user_id' => 4,
                'category_leave_id' => 1,
                'days' => 3,
                'leave_date' => '2024-01-15',
                'explaination' => 'Jalan-Jalan',
                'evidence' => null,
                'flag' => 'approved',
                'verified_by' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => \Str::uuid()->toString(),
                'user_id' => 4,
                'category_leave_id' => 1,
                'days' => 1,
                'leave_date' => '2024-01-15',
                'explaination' => 'Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle quora plaxo ideeli hulu weebly balihoo...',
                'evidence' => null,
                'flag' => 'approved',
                'verified_by' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => \Str::uuid()->toString(),
                'user_id' => 4,
                'category_leave_id' => 1,
                'days' => 2,
                'leave_date' => '2024-01-15',
                'explaination' => 'Hiling',
                'evidence' => null,
                'flag' => 'approved',
                'verified_by' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [ // Cuti Kemalangan Dukacita Keluarga
                'id' => \Str::uuid()->toString(),
                'user_id' => 4,
                'category_leave_id' => 5,
                'days' => 3,
                'leave_date' => '2024-01-15',
                'explaination' => 'Nolo',
                'evidence' => null,
                'flag' => 'approved',
                'verified_by' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [ // Cuti Kemalangan Merawat Keluarga
                'id' => \Str::uuid()->toString(),
                'user_id' => 4,
                'category_leave_id' => 6,
                'days' => 1,
                'leave_date' => '2024-01-15',
                'explaination' => 'Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle quora plaxo ideeli hulu weebly balihoo...',
                'evidence' => null,
                'flag' => 'approved',
                'verified_by' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [ // Cuti Menikah
                'id' => \Str::uuid()->toString(),
                'user_id' => 4,
                'category_leave_id' => 7,
                'days' => 2,
                'leave_date' => '2024-01-15',
                'explaination' => 'Menikah yang ke sekian x',
                'evidence' => null,
                'flag' => 'approved',
                'verified_by' => 3,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [ // Cuti Hamil
                'id' => \Str::uuid()->toString(),
                'user_id' => 4,
                'category_leave_id' => 8,
                'days' => 3,
                'leave_date' => '2024-01-15',
                'explaination' => 'Bini Hamil',
                'evidence' => null,
                'flag' => 'submit',
                'verified_by' => null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];
        PaidLeave::insert($paid_leave);
    }
}
