<?php

namespace Database\Seeders;

use App\Models\PaidLeaveCat;
use Illuminate\Database\Seeder;

class PaidLeaveCatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cat = [
            ['name' => 'Cuti Tahunan','limit' => 12, 'parent_id' => 0],
            ['name' => 'Cuti Sakit','limit' => 0, 'parent_id' => 0],
            ['name' => 'Cuti Alasan Penting', 'limit' => 0, 'parent_id' => 0],

            ['name' => 'Cuti Izin Sakit (Rawat Inap)', 'limit' => 0, 'parent_id' => 3],
            ['name' => 'Cuti Kemalangan Dukacita Keluarga(inti)', 'limit' => 3, 'parent_id' => 3],
            ['name' => 'Cuti Kemalangan Merawat Keluarga(inti) Sakit', 'limit' => 3, 'parent_id' => 3],
            ['name' => 'Cuti Menikah', 'limit' => 3, 'parent_id' => 3],
            ['name' => 'Cuti Hamil', 'limit' => 3, 'parent_id' => 3],
        ];
        PaidLeaveCat::insert($cat);
    }
}
