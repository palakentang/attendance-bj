<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'  => 'superadmin',
            'email' => 'superadmin@mail.com',
            'password' => bcrypt('secret'),
            'tenant_id' => 1,
        ]);

        $role = Role::create(['name' => 'superadmin']);

        Permission::pluck('id','id')->all();

        $user->givePermissionTo(
            [
                'role-list',
                'role-create',
                'role-edit',
                'role-delete',

                // Absensi
                'attendance-list',
                'attendance-create',
                'attendance-edit',
                'attendance-delete',

                // Cuti
                'leave-list',
                'leave-create',
                'leave-edit',
                'leave-delete',

                // Dinas
                'dinas-list',
                'dinas-create',
                'dinas-edit',
                'dinas-delete',

                // User
                'user-list',
                'user-create',
                'user-edit',
                'user-delete',

                // Permission
                'permission-list',
                'permission-create',
                'permission-edit',
                'permission-delete',

            ]
        );

        $user->assignRole([$role->id]);

        $user_bj = User::create([
            'name'  => 'superadmin',
            'email' => 'superadmin@bj.com',
            'password' => bcrypt('secret'),
            'tenant_id' => 2,
        ]);

        Permission::pluck('id','id')->all();

        $user_bj->givePermissionTo(
            [
                'role-list',
                'role-create',
                'role-edit',
                'role-delete',

                // Absensi
                'attendance-list',
                'attendance-create',
                'attendance-edit',
                'attendance-delete',

                // Cuti
                'leave-list',
                'leave-create',
                'leave-edit',
                'leave-delete',

                // Dinas
                'dinas-list',
                'dinas-create',
                'dinas-edit',
                'dinas-delete',

                // User
                'user-list',
                'user-create',
                'user-edit',
                'user-delete',

                // Permission
                'permission-list',
                'permission-create',
                'permission-edit',
                'permission-delete',

            ]
        );

        $user_bj->assignRole([$role->id]);

        /** ADMIN USER */

        $admin = User::create([
            'name'  => 'admin',
            'email' => 'admin@mail.com',
            'password' => bcrypt('secret'),
            'tenant_id' => 1,
        ]);


        $role_admin = Role::create(['name' => 'admin']);

        $admin->givePermissionTo(
            [
                'leave-list','leave-create','leave-edit',
                'dinas-list','dinas-create',
                'attendance-list','attendance-create','dinas-edit','dinas-delete',
            ]
        );

        $admin->assignRole([$role_admin->id]);
    }
}
