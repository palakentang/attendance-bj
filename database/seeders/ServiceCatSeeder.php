<?php

namespace Database\Seeders;

use App\Models\ServicesCat;
use Illuminate\Database\Seeder;

class ServiceCatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service_cat = [
            ['name' => 'Dinas Luar Awal', 'time_start' => '08:00', 'time_end' => '14:00'],
            ['name' => 'Dinas Luar Akhir', 'time_start' => '14:00', 'time_end' => '16:00'],
            ['name' => 'Dinas Luar Penuh', 'time_start' => '08:00', 'time_end' => '16:00'],
        ];
        ServicesCat::insert($service_cat);
    }
}
