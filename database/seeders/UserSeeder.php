<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staff = User::create([
            'name'  => 'staff',
            'email' => 'staff@mail.com',
            'password' => bcrypt('secret'),
            'tenant_id' => 1
        ]);

        $role_staff = Role::create(['name' => 'staff']);

        $staff->givePermissionTo(
            [
                'leave-list','leave-create','leave-edit','leave-delete',
                'dinas-list','dinas-create','dinas-edit','dinas-delete',
                'attendance-list','attendance-create','dinas-edit','dinas-delete',
            ]
        );

        $staff->assignRole([$role_staff->id]);

        /** PJLP USER */

        $pjpl_users = [
            [
                'name'  => 'pjlp',
                'email' => 'pjlp@mail.com',
                'password' => bcrypt('secret'),
                'tenant_id' => 1
            ],[
                'name'  => 'pjlp-2',
                'email' => 'pjlp-2@mail.com',
                'password' => bcrypt('secret'),
                'tenant_id' => 1
            ],[
                'name'  => 'pjlp-3',
                'email' => 'pjlp-3@mail.com',
                'password' => bcrypt('secret'),
                'tenant_id' => 1
            ],[
                'name'  => 'pjlp-4',
                'email' => 'pjlp-4@mail.com',
                'password' => bcrypt('secret'),
                'tenant_id' => 1
            ]
        ];
        $role_pjlp = Role::create(['name' => 'pjlp']);

        foreach ($pjpl_users as $pjlp_user) {
            $pjlp = User::create($pjlp_user);
            $pjlp->givePermissionTo(
                [
                    'leave-list','leave-create',
                    'dinas-list','dinas-create',
                    'attendance-list','attendance-create','dinas-edit','dinas-delete',
                ]
            );
            $pjlp->assignRole([$role_pjlp->id]);
        }

        $role_pjlp = Role::create(['name' => 'mobile-attendance']);
// Berita Jakarta
        $staff_bj = User::create([
            'name'  => 'staff berita jakarta',
            'email' => 'staff@bj.com',
            'password' => bcrypt('secret'),
            'tenant_id' => 2
        ]);

        $staff_bj->givePermissionTo(
            [
                'leave-list','leave-create','leave-edit','leave-delete',
                'dinas-list','dinas-create','dinas-edit','dinas-delete',
                'attendance-list','attendance-create','dinas-edit','dinas-delete',
            ]
        );

        $staff_bj->assignRole([$role_staff->id]);

        /** PJLP USER */

        $pjpl_users_bjs = [
            [
                'name'  => 'pjlp',
                'email' => 'pjlp@bj.com',
                'password' => bcrypt('secret'),
                'tenant_id' => 2
            ],[
                'name'  => 'pjlp-2',
                'email' => 'pjlp-2@bj.com',
                'password' => bcrypt('secret'),
                'tenant_id' => 2
            ],[
                'name'  => 'pjlp-3',
                'email' => 'pjlp-3@bj.com',
                'password' => bcrypt('secret'),
                'tenant_id' => 2
            ],[
                'name'  => 'pjlp-4',
                'email' => 'pjlp-4@bj.com',
                'password' => bcrypt('secret'),
                'tenant_id' => 2
            ]
        ];

        foreach ($pjpl_users_bjs as $pjpl_users_bj) {
            $pjlp = User::create($pjpl_users_bj);
            $pjlp->givePermissionTo(
                [
                    'leave-list','leave-create',
                    'dinas-list','dinas-create',
                    'attendance-list','attendance-create','dinas-edit','dinas-delete',
                ]
            );
            $pjlp->assignRole([$role_pjlp->id]);
        }

        // $role_pjlp = Role::create(['name' => 'mobile-attendance']);

    }
}
