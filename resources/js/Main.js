import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Index from "./pages/dinas/Index"
import Create from "./pages/dinas/Create"
import Edit from "./pages/dinas/Edit"
import Show from "./pages/dinas/Show"
   
function Main() {
    return (
        <Router>
            <Routes>
                <Route exact path="/"  element={<Index/>} />
                <Route path="/create"  element={<Create/>} />
                <Route path="/edit/:id"  element={<Edit/>} />
                <Route path="/show/:id"  element={<Show/>} />
            </Routes>
        </Router>
    );
}
   
export default Main;
   
if (document.getElementById('app')) {
    ReactDOM.render(<Main />, document.getElementById('app'));
}