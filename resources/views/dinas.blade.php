@extends('layouts.app-lte')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @include('components.alert')
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{ucfirst($pageTitle)}}</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        @foreach ($data as $item)

                        <div class="post">
                            <div class="user-block">
                              <img class="img-circle img-bordered-sm" src="{{asset('v2/dist/img/AdminLTELogo.png')}}" alt="user image">
                              <span class="username">
                                <a href="#">{{ucfirst($item->CommissionedBy->name)}}</a>
                                <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                              </span>
                              <span class="description">Dibuat pada {{date_humans(date('Y-m-d', strtotime($item->created_at)))}}</span>
                            </div>
                            <!-- /.user-block -->
                            <h4>Nomor Surat: {{$item->name}}</h4>
                            <p>
                              {{$item->service_description}}
                              <table style="width:100%">
                                <tr>
                                    <th>Tanggal Dinas:</th>
                                    <td>{{date_humans($item->service_date_start)}} s/d {{date_humans($item->service_date_end)}}</td>
                                </tr>
                                <tr>
                                    <th>Yang Ditugaskan:</th>
                                    <td>
                                        <ul style="margin-inline-start: -20px;">
                                            @foreach ($item->Details as $detail)
                                            <li>{{$detail->user->name}}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Surat Dinas</th>
                                    <td><a target="_blank" href="{{url('uploads/dinas/'.date('Y-m-d').'/'.$item->letter_of_assignment)}}">Disini</a></td>
                                </tr>
                                </table>
                            </p>

                            {{-- @dd($item->Details->where('user_id', auth()->user()->id)->first()); --}}
                            @if ($item->Details->where('user_id', auth()->user()->id)->first()->flag == 'submit')
                            <p>
                              <a href="#" onclick="event.preventDefault(); document.getElementById('frm-approve-dinas{{$item->id}}').submit();" class="btn btn-success btn-block text-sm"><i class="far fa-thumbs-up mr-1"></i> Terima</a>
                              <form id="frm-approve-dinas{{$item->id}}" action="{{ route('dinas.store') }}" method="POST" style="display:none;">
                                {{ csrf_field() }}
                                <input type="text" name="dinas_id" value="{{$item->id}}" />
                              </form>
                            </p>
                            @endif

                          </div>
                          <!-- /.post -->
                        @endforeach
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        Footer
                    </div>
                    <!-- /.card-footer-->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
@section('js-section')

@endsection
