<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{asset('v2/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">YANIP</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('v2/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2"
                    alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{auth()->user()->name}}</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                    aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
                @hasrole('staff|superadmin')
                <li class="nav-item">
                    <a href="{{route('staff.dashboard')}}" class="nav-link {{$pageTitle == 'Dashboard' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard {{auth()->user()->hasRole('superadmin') ? '(Staff)' : ''}}
                        </p>
                    </a>
                </li>
                @endhasrole
                @hasrole('pjlp|superadmin')
                <li class="nav-item">
                    <a href="{{route('dashboard')}}" class="nav-link {{$pageTitle == 'Dashboard' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                @endhasrole
                @hasrole('mobile-attendance|superadmin')
                <li class="nav-item">
                    <a href="{{route('attendance.index')}}" class="nav-link {{$pageTitle == 'Absent' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-camera"></i>
                        <p>
                            Presensi
                        </p>
                    </a>
                </li>
                @endhasrole

                @hasrole('staff|superadmin|admin')
                <li class="nav-item">
                    <a href="{{route('admin.attendances.index')}}" class="nav-link {{$pageTitle == 'Manajemen Absensi' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-camera"></i>
                        <p>
                            Presensi {{auth()->user()->hasRole('superadmin') ? '(Staff)' : ''}}
                        </p>
                    </a>
                </li>
                @endhasrole

                @can('permission-list')
                <li class="nav-item {{$pageTitle == 'User Manajemen' || $pageTitle == 'Role Manajemen' || $pageTitle == 'Permission Manajemen' ? 'menu-open' : ''}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            User Manajemen
                            @can('permission-list')
                                <i class="right fas fa-angle-left"></i>
                            @endcan
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('user-list')
                        <li class="nav-item">
                            <a href="{{route('users.index')}}" class="nav-link {{$pageTitle == 'User Manajemen' ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>User </p>
                            </a>
                        </li>
                        @endcan
                        @can('role-list')
                        <li class="nav-item">
                            <a href="{{route('roles.index')}}" class="nav-link {{$pageTitle == 'Role Manajemen' ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Role</p>
                            </a>
                        </li>
                        @endcan
                        @can('permission-list')
                        <li class="nav-item">
                            <a href="{{route('permissions.index')}}" class="nav-link {{$pageTitle == 'Permission Manajemen' ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Permission</p>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>
                @endcan

                @hasrole('pjlp|superadmin|admin')
                <li class="nav-item">
                    <a href="{{route('paid-leaves.index')}}" class="nav-link {{$pageTitle == 'Cuti' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>
                            Cuti <span class="right badge badge-danger" id="notifikasi-pengajuan-cuti">{{notification(auth()->user(), 'paid-leave')}}</span>
                        </p>
                    </a>
                </li>
                @endhasrole
                @hasrole('pjlp|superadmin')
                <li class="nav-item">
                    <a href="{{route('dinas.index')}}" class="nav-link {{$pageTitle == 'dinas' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>
                            Dinas <span class="right badge badge-danger" id="notifikasi-pengajuan-cuti">{{notification(auth()->user(), 'dinas')}}</span>
                        </p>
                    </a>
                </li>
                @endhasrole
                @hasrole('staff|superadmin')
                <li class="nav-item menu-is-opening menu-open">
                    <a href="#" class="nav-link">
                      <i class="nav-icon fas fa-tachometer-alt"></i>
                      <p>
                        Cuti
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview" style="display: block;">
                      <li class="nav-item">
                        <a href="{{route('admin.paid-leaves.index')}}" class="nav-link {{$pageTitle == 'Cuti' ? 'active' : ''}}">
                          <i class="far fa-circle nav-icon"></i>
                          <p>
                            Pengajuan {{auth()->user()->hasRole('superadmin') ? '(Staff)' : ''}} <span class="right badge badge-danger" id="notifikasi-pengajuan-cuti">{{notification(auth()->user(), 'paid-leave')}}</span>
                          </p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{route('admin.paid-leaves.users.lists')}}" class="nav-link {{$pageTitle == 'Cuti Users' ? 'active' : ''}}">
                          <i class="far fa-circle nav-icon"></i>
                          <p>Users</p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="{{route('admin.sick-leaves.index')}}" class="nav-link {{$pageTitle == 'Cuti Sakit' ? 'active' : ''}}">
                          <i class="far fa-circle nav-icon"></i>
                          <p>
                            Sakit {{auth()->user()->hasRole('superadmin') ? '(Staff)' : ''}} <span class="right badge badge-danger" id="notifikasi-pengajuan-cuti">{{notification(auth()->user(), 'sick')}}</span>
                          </p>
                        </a>
                      </li>
                    </ul>
                  </li>
                <li class="nav-item">
                    <a href="{{route('admin.dinas.index')}}" class="nav-link {{$pageTitle == 'dinas' ? 'active' : ''}}">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>
                            Dinas {{auth()->user()->hasRole('superadmin') ? '(Staff)' : ''}} <span class="right badge badge-danger" id="notifikasi-pengajuan-cuti">{{notification(auth()->user(), 'dinas')}}</span>
                        </p>
                    </a>
                </li>
                @endhasrole
                <li class="nav-item">
                    @component('components.logout', [
                        'class' => 'nav-link',
                        'icon' => 'nav-icon fas fa-sign-out'
                    ])

                    @endcomponent
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
