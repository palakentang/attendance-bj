<!DOCTYPE html>
<html lang="en">

<!-- Sistem MAnajemen Learning And School-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="https://masfhr.info">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="https://www.multipurposethemes.com/admin/eduadmin-template/images/favicon.ico">

    <title>{{ $pageTitle }}</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('v2/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('v2/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('v2/dist/css/adminlte.min.css') }}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <script src="{{ asset('v2/plugins/jquery/jquery.min.js') }}"></script>

    @yield('css-section')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
        <!-- Navbar -->
        @include('layouts.module.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('layouts.module.sidebar')
        <!-- /.Main Sidebar Container -->


        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->

            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>{{ ucfirst($pageTitle) }}</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">{{ ucfirst($pageTitle) }}</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            @yield('content')

            <div class="modal fade" id="modal-change-password">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title">Ganti Password </h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                         {!! Form::open(['method' => 'post', 'route' => auth()->user()->hasRole('pjlp') ? 'update-password' : 'admin.update-password']) !!}
                         <div class="mb-3">
                            <label for="oldPasswordInput" class="form-label">Old Password</label>
                            <input name="old_password" type="password" class="form-control @error('old_password') is-invalid @enderror" id="oldPasswordInput"
                                placeholder="Old Password">
                            @error('old_password')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="newPasswordInput" class="form-label">New Password</label>
                            <input name="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" id="newPasswordInput"
                                placeholder="New Password">
                            @error('new_password')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="confirmNewPasswordInput" class="form-label">Confirm New Password</label>
                            <input name="new_password_confirmation" type="password" class="form-control" id="confirmNewPasswordInput"
                                placeholder="Confirm New Password">
                        </div>
                         <div class="form-group">
                            {!! Form::submit('Simpan', ['class' => 'btn btn-block btn-success']) !!}
                        </div>
                         {!! Form::close() !!}

                    </div>
                    <div class="card-footer"></div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
              <!-- /.modal -->

        </div>
        <!-- /.content-wrapper -->

        @include('layouts.module.footer')


    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->

    <!-- Bootstrap 4 -->
    <script src="{{ asset('v2/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- overlayScrollbars -->
    <script src="{{ asset('v2/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('v2/dist/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('v2/dist/js/demo.js') }}"></script>
    <script src="{{ asset('v2/dist/js/jqClock.min.js') }}"></script>
    <script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>

    <script>
        $(document).ready(function() {
            $("#jam").clock({
                "format": "24",
                "calendar": "false"
            });
        });

        function changePassword(user_id) {
            $('#modal-change-password').modal('show')
        }

        // Enable pusher logging - don't include this in production
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('d298640228c2f3ac76ff', {
            cluster: 'ap1'
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function(data) {
            console.log(data);
            // $('#example1').DataTable().ajax.reload();
            // $('#counting-notification').html(data.message.length + ' Notifikasi Baru')
            // $('#badge-counting-notification').html(data.message.length)
            // // Objek untuk menyimpan hasil pengelompokan dan jumlah
            // var groupedData = {};

            // // Iterasi melalui array data
            // for (var index = 0; index < data.message.length; index++) {
            //     var element = data.message[index];
            //     var notifableType = element.notifable_type;

            //     // Buat kelompok baru jika belum ada
            //     if (!groupedData[notifableType]) {
            //         groupedData[notifableType] = 0;
            //     }

            //     // Tambahkan jumlah
            //     groupedData[notifableType]++;
            // }

            // // Iterasi melalui hasil pengelompokan
            // $.each(groupedData, function(notifableType, count) {
            //     // Buat HTML berdasarkan hasil
            //     // var html = '<p>' + notifableType + ': ' + count + '</p>';

            //     // Tampilkan hasil di dalam container
            //     // $('#result-container').append(html);
            //     $('#notifikasi-pengajuan-cuti').html(count)
            //     $('#notifikasi-pengajuan-cuti').show()

            // });
        });
    </script>

    @yield('js-section')

</body>

</html>
