<a onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="{{$class}}" href="javascript:void(0)"><i class="{{$icon}}"></i> Logout</a>
<form id="frm-logout" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
