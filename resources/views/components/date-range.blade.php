<div class="form-group">
  <strong>{{$label}}</strong>
  <div class="input-group">
      <div class="input-group-prepend">
        <span class="input-group-text">
          <i class="far fa-calendar-alt"></i>
        </span>
      </div>
      <input type="text" class="form-control float-right" id="daterangecustom" name="{{$name}}" required>
    </div>
</div>