@extends('layouts.app-lte')
@section('css-section')
    <link rel="stylesheet" href="{{ asset('v2/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('v2/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('v2/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <style>
        .info-box:hover {
            -webkit-box-shadow: 1px 10px 12px -10px rgba(0, 0, 0, 0.95);
            -moz-box-shadow: 1px 10px 12px -10px rgba(0, 0, 0, 0.95);
            box-shadow: 1px 6px 20px -15px rgba(0, 0, 0, 0.95);
            border: 1px solid;
            cursor: pointer;
        }

        .info-box:active {
            -webkit-box-shadow: 1px 10px 12px -10px rgba(0, 0, 0, 0.95);
            -moz-box-shadow: 1px 10px 12px -10px rgba(0, 0, 0, 0.95);
            box-shadow: 1px 6px 20px -15px rgba(0, 0, 0, 0.95);
            border: 1px solid;
        }

        .row-paid-leaves {
            display: flex;
            justify-content: space-between;
        }

        .col-sm-3 {
            border: solid 1px black;
            width: 25%;
        }

        .loader {
            border: 16px solid #f3f3f3;
            /* Light grey */
            border-top: 16px solid #3498db;
            /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            margin: auto;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @include('components.alert')
            <div class="row">
                <div class="col-md-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List User Mengajukan Cuti Sakit</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="d-none" border="0" cellspacing="5" cellpadding="5">
                                <tbody>
                                    <tr id="tr_month">
                                        <td>Pencarian Bulanan</td>
                                        <td>
                                            <select name="month" id="month">
                                                <option value="0">Pilih Bulan</option>
                                                <option value="1">Januari</option>
                                                <option value="2">Februari</option>
                                                <option value="3">Maret</option>
                                                <option value="4">April</option>
                                                <option value="5">Mei</option>
                                                <option value="6">Juni</option>
                                                <option value="7">Juli</option>
                                                <option value="8">Agustus</option>
                                                <option value="9">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="tr_month">
                                        <td>Pencarian Status</td>
                                        <td>
                                            <select name="status" id="status">
                                                <option value="">Pilih Status</option>
                                                <option value="submit">Mengajukan</option>
                                                <option value="approved">Disetujui</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nama User</th>
                                        <th>Alasan</th>
                                        <th>Lama Hari</th>
                                        <th>Tanggal Pengajuan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($sicks as $sick)
                                    <tr>
                                        <td>{{$sick->User->name}}</td>
                                        <td>{{$sick->explaination}}</td>
                                        <td>{{$sick->days}} Hari</td>
                                        <td>{{date_humans($sick->leave_date)}}</td>
                                        <td>
                                            <a onclick="detail('{{$sick->id}}', '{{$sick->days}}', '{{date_humans($sick->leave_date)}}')" href="javascript:void(0)">Detail</a>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="6"><center>Tidak Ada Izin Sakit Hari ini</center></td>
                                    </tr>
                                    @endforelse

                                </tbody>
                            </table>

                        </div>
                    </div>
                    <!-- /.card -->


                </div>
            </div>
            <div class="modal fade" id="modal-submit-sick">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Form Persetujuan Cuti Sakit</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('admin.sick-leaves.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="sick_leave_id" />
                                <input type="hidden" name="flag" />
                                <div class="form-group">
                                    <label for="">Surat Sakit</label>
                                    <input type="file" name="evidance" id="file-evidance" class="form-control">
                                </div>
                                <img src="" alt="" srcset="" id="evidance" class="img-thumbnail d-none">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="">Tanggal Pengajuan</label>
                                            <input id="list_tanggal_pengajuan" type="text" readonly class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="">Lama Cuti Sakit</label>
                                            <input id="days" type="text" readonly class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">Tanggal Sakit</label>
                                    <input type="date" name="fix_date" class="form-control">
                                </div>

                                <div class="row">
                                    <div class="col-6">
                                        <input type="submit" value="setuju" name="flag" class="btn btn-block btn-success">
                                    </div>
                                    <div class="col-6">
                                        <input type="submit" value="tolak" name="flag" class="btn btn-block btn-danger">
                                    </div>
                                </div>
                            </form>
                        </div>
                        {{-- <div class="modal-footer justify-content-between">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div> --}}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
    </section>
    <!-- /.content -->
@endsection
@section('js-section')
    <script src="{{ asset('v2/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url =  "{{ route('admin.paid-leaves.lists') }}";
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "pageLength": 10,
            "searching": true,
        });

        function detail(sick_leave_id, days, sick_start_date) {
            if (confirm('Yakin Setuju?')) {
                $('#modal-submit-sick').modal()
                $('input[name=sick_leave_id]').val(sick_leave_id)
                $('#list_tanggal_pengajuan').val(sick_start_date)
                $('#days').val(days+' Hari')
            }
            return;
        }

        function tolak(user_id, category_leave_id, leave_date, status = 'decline') {
            if (confirm('Yakin Tolak?')) {
                $('#modal-submit-sick').modal()
            }
            return;
        }

        function readURL(input) {
            console.log(input.target);
            if (input.target.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#evidance').removeClass('d-none');
                    $('#evidance').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.target.files[0]);
            }
        }

        $("#file-evidance").change(function(e){
            readURL(e);
        });
    </script>
@endsection
