@extends('layouts.app-lte')
@section('css-section')
    <link rel="stylesheet" href="{{ asset('v2/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('v2/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('v2/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <style>
        .info-box:hover {
            -webkit-box-shadow: 1px 10px 12px -10px rgba(0, 0, 0, 0.95);
            -moz-box-shadow: 1px 10px 12px -10px rgba(0, 0, 0, 0.95);
            box-shadow: 1px 6px 20px -15px rgba(0, 0, 0, 0.95);
            border: 1px solid;
            cursor: pointer;
        }

        .info-box:active {
            -webkit-box-shadow: 1px 10px 12px -10px rgba(0, 0, 0, 0.95);
            -moz-box-shadow: 1px 10px 12px -10px rgba(0, 0, 0, 0.95);
            box-shadow: 1px 6px 20px -15px rgba(0, 0, 0, 0.95);
            border: 1px solid;
        }

        .row-paid-leaves {
            display: flex;
            justify-content: space-between;
        }

        .col-sm-3 {
            border: solid 1px black;
            width: 25%;
        }

        .loader {
            border: 16px solid #f3f3f3;
            /* Light grey */
            border-top: 16px solid #3498db;
            /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            margin: auto;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
@endsection
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @include('components.alert')
            <div class="row">
                <div class="col-md-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">List User Mengajukan Cuti</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table border="0" cellspacing="5" cellpadding="5">
                                <tbody>
                                    <tr id="tr_month">
                                        <td>Pencarian Bulanan</td>
                                        <td>
                                            <select name="month" id="month">
                                                <option value="0">Pilih Bulan</option>
                                                <option value="1">Januari</option>
                                                <option value="2">Februari</option>
                                                <option value="3">Maret</option>
                                                <option value="4">April</option>
                                                <option value="5">Mei</option>
                                                <option value="6">Juni</option>
                                                <option value="7">Juli</option>
                                                <option value="8">Agustus</option>
                                                <option value="9">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr id="tr_month">
                                        <td>Pencarian Status</td>
                                        <td>
                                            <select name="status" id="status">
                                                <option value="">Pilih Status</option>
                                                <option value="submit">Mengajukan</option>
                                                <option value="approved">Disetujui</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nama User</th>
                                        <th>Jenis Cuti</th>
                                        <th>Alasan Cuti</th>
                                        <th>Tanggal Pengajuan Cuti</th>
                                        <th>Tanggal Cuti</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                    <!-- /.card -->


                </div>
            </div>
            <div class="modal fade" id="modal-default">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Riwayat Cuti</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="loader" style="display: none;" id="loader-categories"></div>

                            <div class="row no-gutters" id="history_paid_leave">


                            </div>

                            <div class="card card-outline card-success" style="display: none;" id="riwayat-cuti">
                                <div class="card-header">
                                    <h3 class="card-title">Riwayat Cuti</h3>


                                    <!-- /.card-tools -->
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="timeline">
                                        <div class="loader" style="display: none;" id="loader-timeline"></div>

                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>

                        </div>
                        {{-- <div class="modal-footer justify-content-between">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div> --}}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
    </section>
    <!-- /.content -->
@endsection
@section('js-section')
    <script src="{{ asset('v2/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url =  "{{ route('admin.paid-leaves.lists') }}";
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{ route('admin.paid-leaves.lists') }}",
                "type": "POST",
                "data": function(data) {
                    data.search = $('input[type="search"]').val();
                    data.month = $('#month').val();
                    data.status = $('#status').val();
                    data._token = '{{csrf_token()}}'
                }
            },
            "order": ['1', 'DESC'],
            "pageLength": 10,
            "searching": true,
            "aoColumns": [{
                    data: 'user.name',
                },
                {
                    data: 'category.name',
                },
                {
                    data: 'explaination',
                },
                {
                    data: 'leave_date_humans',
                },
                {
                    data: 'lists_date_leave',
                },
                {
                    data: 'id',
                    width: "20%",
                    render: function(data, type, row) {
                        return `
                                    ${row.flag == 'submit' ? `<button onclick="approve(${row.user_id}, ${row.category_leave_id}, '${row.leave_date}')" type="button" class="btn btn-success"><i class="fas fa-check"></i></button>&nbsp;<button onclick="decline(${row.user_id}, ${row.category_leave_id}, '${row.leave_date}')" type="button" class="btn btn-danger"><i class="fas fa-times"></i></button>` : ''}
                                    <button onclick="history(${row.user_id})" type="button" class="btn btn-primary"><i class="fas fa-history"></i></button>
                                    `; //you can add your view route here
                    }
                }
            ],
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        function approve(user_id, category_leave_id, leave_date, status = 'approve') {
            if (confirm('Are you sure?')) {
                $.post("{{ route('admin.paid-leaves.store') }}", {
                        user_id,
                        category_leave_id,
                        leave_date,
                        status
                    },
                    function(data, textStatus, jqXHR) {
                        $('#example1').DataTable().draw(false);
                        alert(data.message)
                    },
                    "JSON"
                );
            }
            return;
        }

        function decline(user_id, category_leave_id, leave_date, status = 'decline') {
            if (confirm('Are you sure?')) {
                $.post("{{ route('admin.paid-leaves.store') }}", {
                        user_id,
                        category_leave_id,
                        leave_date,
                        status
                    },
                    function(data, textStatus, jqXHR) {
                        $('#example1').DataTable().draw(false);
                        alert(data.message)
                    },
                    "JSON"
                );
            }
            return;
        }

        function history(user_id) {
            $('#loader-categories').show();
            var url = `{{ route('admin.paid-leaves.show', ':slug') }}`
            url = url.replace(':slug', user_id)
            $('#modal-default').modal('show')

            $.get(url,
                function(data, textStatus, jqXHR) {
                    $('#modal-default').modal('show')
                    var html = ``;
                    for (let index = 0; index < Object.keys(data.amount_of_leave_paid).length; index++) {
                        const element = data.amount_of_leave_paid;
                        const paid_leave_name = Object.keys(data.amount_of_leave_paid)[index]
                        const flag = element.flag == 'submit' ? 'primary' : element.flag == 'approved' ? 'success' :
                            'danger'
                        const flag_text = element.flag == 'submit' ? 'Mengajukan' : element.flag == 'approved' ?
                            'Disetujui' : 'Ditolak'
                        const button_flag_cuti = `<span class="badge badge-${flag}">${flag_text}</span>`
                        const button_download_surat_cuti = element.flag == 'approved' ?
                            `<button onclick="event.preventDefault(); document.getElementById('${element.id}').submit();" class="btn btn-primary btn-sm">Download Surat Cuti</button>` :
                            ''
                        const button_cancel_surat_cuti = element.flag == 'submit' ?
                            `<span onclick="cancel('${element.id}')" style="float: right; margin: 3px" class="btn btn-danger btn-sm"><i class="fas fa-times"></i> Batalkan</span>` :
                            '';
                        const paid_leave_name_arr = {yearly: 'Tahunan', mount: 'Duka Cita', caring: 'Merawat', married: 'Menikah', pregnant: 'Menikah'}
                        html += `<div class="col" style="margin-inline-end: 10px;">
                                <div class="info-box" onclick="detail_paid_leave('${paid_leave_name}', ${user_id})">
                                    <span class="info-box-icon bg-success"><i class="fas fa-history"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">${translate_paid_leave(paid_leave_name)}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                            </div>`;
                        $('#history_paid_leave').html(html);
                    }
                    $('#loader-categories').hide();
                    $('#riwayat-cuti').show();
                },
                "JSON"
            );
        }

        function translate_paid_leave(string) {
            const arr = {yearly: 'Tahunan', mourn: 'Duka Cita', caring: 'Merawat', married: 'Menikah', pregnant: 'Menikah'}
            return arr[string];
        }

        function detail_paid_leave(paid_leave_name, user_id) {
            $('#loader-timeline').show();
            var url = "{{ route('admin.paid-leaves.edit', ':slug') }}";
            url = url.replace(':slug', paid_leave_name)
            $('.timeline').html('')
            $.get(url, {
                    paid_leave_name,
                    user_id
                },
                function(data, textStatus, jqXHR) {
                    var html = ``;
                    for (let index = 0; index < data.length; index++) {
                        const element = data[index];
                        const flag = element.flag == 'submit' ? 'primary' : element.flag == 'approved' ? 'success' :
                            'danger'
                        const flag_text = element.flag == 'submit' ? 'Mengajukan' : element.flag == 'approved' ?
                            'Disetujui' : 'Ditolak'
                        const button_flag_cuti = `<span class="badge badge-${flag}">${flag_text}</span>`
                        html += `
                                <div class="time-label">
                                <span class="bg-${flag}">${element.submit_leave_day}</span>
                                </div>

                                <div>
                                    <i class="fas fa-history bg-blue"></i>
                                    <div class="timeline-item">
                                        <span class="time"><i class="fas fa-clock"></i> 12:05</span>
                                        <h3 class="timeline-header">Status : ${button_flag_cuti}</h3>

                                        <div class="timeline-body">
                                            ${element.explaination}
                                            <br>Banyak Cuti: <strong>${element.days} Hari</strong>
                                            <br>Tanggal Cuti: <strong>${element.lists_date_leave}</strong>
                                        </div>
                                        <div class="timeline-footer">
                                        </div>
                                    </div>
                                </div>`;
                        $('.timeline').html(html);
                    }
                    $('#loader-timeline').hide();
                },
                "JSON"
            );
        }

        let table = new DataTable('#example1');

        $('#month').on('change', function(e) {
            e.preventDefault();
            table.draw()
        });

        $('#status').on('change', function(e) {
            e.preventDefault();
            table.draw()
        });
    </script>
@endsection
