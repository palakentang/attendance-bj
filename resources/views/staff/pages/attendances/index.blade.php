@extends('layouts.app-lte')
@section('css-section')
<link rel="stylesheet" href="{{ asset('v2/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('v2/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('v2/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/datetime/1.5.1/css/dataTables.dateTime.min.css">
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
    integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin="" />
<script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
    integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>
<style>
    .leaflet-container {
        height: 400px;
        width: 600px;
        max-width: 100%;
        max-height: 100%;
    }
</style>
@endsection
@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @include('components.alert')
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Absensi</h3>

                        <div class="card-tools">
                            <button type="button" onclick="importAttendance()" class="btn btn-success" title="Import">
                                <i class="fas fa-download"></i>
                            </button>
                            <a onclick="exportAttendance()" class="btn btn-success" title="Export">
                                <i class="fas fa-upload"></i>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        {{-- <div id="map"></div> --}}
                        <table border="0" cellspacing="5" cellpadding="5">
                            <tbody>
                                <tr id="tr_month">
                                    <td>Pencarian Bulanans</td>
                                    <td>
                                        <select name="month" id="month" >
                                            <option value="0">Pilih Bulan</option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr id="tr_date">
                                    <td>Pencarian Tanggal:</td>
                                    <td><input type="date" id="min" name="min"></td>
                                </tr>
                                <tr>
                                    <td>Pencarian Nama:</td>
                                    <td><input type="text" id="search" name="search"></td>
                                </tr>
                            </tbody>
                        </table>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nama User</th>
                                    <th>Tanggal</th>
                                    <th>Masuk (Jam, Lokasi)</th>
                                    <th>Pulang (Jam, Lokasi)</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>

                    </div>
                </div>
                <!-- /.card -->


            </div>
            <div class="modal fade" id="modal-default">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Import File</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(array('route' => 'attendance.export','method'=>'GET', 'id' => 'frm-export')) !!}
                                {!! Form::select('month', getMonth()+[null => 'Pilih Bulan'], '', ['class' => 'form-select']) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button onclick="event.preventDefault(); document.getElementById('frm-export').submit();" type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <div class="modal fade" id="import-modal">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Import User</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        {!! Form::open(array('route' => 'attendance.import','method'=>'POST', 'enctype' =>
                        'multipart/form-data')) !!}
                        <div class="modal-body">
                            <div class="form-group">
                                <strong>File:</strong>
                                {!! Form::file('file_import', null, array('placeholder' => 'Name', 'id' => 'name',
                                'class' => '')) !!}
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
</section>
<!-- /.content -->
@endsection
@section('js-section')
<script src="{{ asset('v2/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('v2/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('v2/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('v2/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>
<script src="https://cdn.datatables.net/datetime/1.5.1/js/dataTables.dateTime.min.js"></script>
<script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var Datatables = $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ajax": {
                "url": "{{ route('admin.attendances.lists') }}",
                "type": "POST",
                "data": function (data) {
                    data.search = $('#search').val();
                    data.min = $('#min').val();
                    data.month = $('#month').val();
                    data.order_by_name = true;
                }
            },
            "pageLength": 10,
            "aoColumns": [
                    {
                        data: 'user.name',
                        name: 'user.name', // Menyertakan nama kolom untuk pengurutan
                        orderable: true, // Membuat kolom ini dapat diurutkan

                    },
                    {
                        data: 'attendance_date_human',
                        orderable: true,
                    },
                    {
                        data: 'attendance_in.attendance_params',
                        render:function(data, type, row){
                            if (data !== undefined) {
                                // console.log(row.user.roles);
                                var isMobileAttendance = row.user.roles.some(function(role) {
                                    return role.name === 'mobile-attendance';
                                });
                            return `<table style="width:100%">
                                    <tr>
                                        <th>Jam</th>
                                        <td>${data.time}</td>
                                    </tr>
                                    <tr>
                                        ${isMobileAttendance ?
                                            `<th>Lokasi</th>
                                            <td>
                                                <a class="btn btn-success" target="_blank" href="https://maps.google.com/?q=${data.lat},${data.long}">Open Map</a>
                                            </td>`:
                                        ``}
                                    </tr>
                                    <tr>
                                        <th>Keterangan Terlambat:</th>
                                        <td><span>${row.is_late}</span></td>
                                    </tr>
                                    <tr>

                                    </tr>
                                    </table>
                                    `;
                                }
                        }
                    },
                    {
                        data: 'attendance_out.attendance_params',
                        render:function(data, type, row){
                            if (data !== undefined) {
                                var isMobileAttendance = row.user.roles.some(function(role) {
                                    return role.name === 'mobile-attendance';
                                });
                                return `<table style="width:100%">
                                    <tr>
                                        <th>Jam</th>
                                        <td>${data.time}</td>
                                    </tr>
                                    <tr>
                                        ${isMobileAttendance ?
                                            `<th>Lokasi</th>
                                            <td>
                                                <a class="btn btn-success" target="_blank" href="https://maps.google.com/?q=${data.lat},${data.long}">Open Map</a>
                                            </td>`:
                                        ``}
                                    </tr>
                                    <tr>
                                        <th>Keterangan Pulang Cepat:</th>
                                        <td><span>${row.is_go_early}</span></td>
                                    </tr>
                                    <tr>

                                    </tr>
                                    </table>
                                    `;
                                }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, row) {
                            if (row  != undefined) {
                                if (row.attendance_out == null) {
                                    if (row.user.paid_leaves.length == 0) {
                                        return `<button class="btn btn-warning" onclick="sick('${row.user_id}', '${row.attendance_date}')">Sakit</button>`
                                    }else{
                                        return 'izin sakit'
                                    }
                                }
                            }

                        }
                    }
            ],
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        function exportAttendance() {
            // const map = L.map('map').setView([lat, long], 99);
            $('#modal-default').modal('show');
        }

        function initMap() {
            if (map != undefined) map.remove();
        }

        function importAttendance() {
            $('#import-modal').modal('show')
        }

        //         let minDate, maxDate;

        // // Custom filtering function which will search data in column four between two values
        // DataTable.ext.search.push(function (settings, data, dataIndex) {
        //     let min = minDate.val();
        //     let max = maxDate.val();
        //     let date = new Date(data[4]);

        //     if (
        //         (min === null && max === null) ||
        //         (min === null && date <= max) ||
        //         (min <= date && max === null) ||
        //         (min <= date && date <= max)
        //     ) {
        //         return true;
        //     }
        //     return false;
        // });

        // Create date inputs
        minDate = new Date('#min', {
            format: 'Y-m-d'
        });
        // maxDate = new DateTime('#max', {
        //     format: 'MMMM Do YYYY'
        // });

        // DataTables initialisation
        let table = new DataTable('#example1');

        // Refilter the table
        document.querySelectorAll('#min').forEach((el) => {
            el.addEventListener('change', () => table.draw());
        });
        document.querySelectorAll('#search').forEach((el) => {
            el.addEventListener('change', () => table.draw());
        })

        $('#month').on('change', function (e) {
            e.preventDefault();
            if ($('#month').find(":selected").val() != '0') {
                $('#min').prop('disabled', true);
            } else {
                $('#min').prop('disabled', false);
            }
            table.draw()

        });

        function sick(user_id, sick_date) {
            $.post("{{route('admin.paid-leaves.store')}}", {user_id, sick_date},
                function (data, textStatus, jqXHR) {
                    if (data.message == 'Gagal, Izin sudah diajukan') {
                        alert(data.message)
                    }
                    table.draw()
                },
                "JSOn"
            );
        }
</script>

@endsection
