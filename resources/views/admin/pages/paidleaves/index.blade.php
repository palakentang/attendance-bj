@extends('layouts.app-lte')
@section('css-section')
<link rel="stylesheet" href="{{asset('v2/dist/css/datepicker.css')}}">
<style>
    .info-box:hover {
        -webkit-box-shadow: 1px 10px 12px -10px rgba(0,0,0,0.95);
        -moz-box-shadow: 1px 10px 12px -10px rgba(0,0,0,0.95);
        box-shadow: 1px 6px 20px -15px rgba(0,0,0,0.95);
        border: 1px solid;
    }
    .info-box:active {
        -webkit-box-shadow: 1px 10px 12px -10px rgba(0,0,0,0.95);
        -moz-box-shadow: 1px 10px 12px -10px rgba(0,0,0,0.95);
        box-shadow: 1px 6px 20px -15px rgba(0,0,0,0.95);
        border: 1px solid;
    }

    .loader {
        border: 16px solid #f3f3f3; /* Light grey */
        border-top: 16px solid #3498db; /* Blue */
        border-radius: 50%;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
        margin: auto;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
</style>

@endsection
@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @include('components.alert')
        <div id="alert-paid-leave" class="alert alert-success alert-dismissible fade show" role="alert" style="display: none;">
            <strong> Berhasil!</strong> Cuti berhasil di terima, selamat Cuti :).
            <button type="button" class="close" data-dismiss="alert" aria-label="Close" style="color:white">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Ajukan Cuti</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('route' => 'paid-leaves.store','method'=>'POST', 'id' =>
                        'form-submit-paid-leave')) !!}
                        @php
                        $data = App\Models\PaidLeaveCat::whereNotIn('id', [2, 3, 4])->pluck('name', 'id')->toArray();
                        @endphp
                        @component('components.select.leave-paid', [
                            'id' => 'leave_paid',
                            'data' => $data
                        ])

                        @endcomponent
                        {{-- <div class="form-group">
                            <strong>Jumlah Hari:</strong>
                            {!! Form::number('days', null, array('placeholder' => 'Jumlah Hari Cuti','class' =>
                            'form-control', 'id' => 'jumlah')) !!}
                        </div> --}}
                        <div class="form-group">
                            <strong>Pilih Hari:</strong>
                            {{-- {!! Form::date('leave_date', '', ['class' => 'form-control date-paid-leave']) !!} --}}
                            <input name="leave_date" type="text" class="form-control date-paid-leave" placeholder="Pilih Tanggal">
                            <span id="remianing_paid_leave" class="text-danger" id=""></span>
                        </div>
                        <div class="form-group">
                            <strong>Alasan:</strong>
                            {!! Form::textarea('explaination', '', ['class' => 'form-control', 'rows' => 3, 'cols' =>
                            2]) !!}
                        </div>
                        <div class="form-group">

                            {!! Form::submit('Simpan', ['class' => 'btn btn-block btn-success']) !!}
                        </div>
                        {!! Form::close() !!}

                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Sisa Cuti</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row d-flex justify-content-center">
                            @foreach ($users as $name => $value)
                            @if ($value['id'] == auth()->user()->id)
                            @foreach ($value['amount_of_leave_paid'] as $key => $item)
                            @php $cat_id = array_search($key, App\Models\PaidLeave::PAID_LEAVE_TYPES)@endphp

                            <div class="col-lg-2 col-md-6 col-xs-12 col-sm-12" style="cursor: pointer;" onclick="detail_paid_leave({{$cat_id}})" id="box-sisa-cuti">
                                <a href="#" style="color: black;">
                                    <div class="info-box" style="text-align: center;">
                                        <div class="info-box-content">
                                            <span class="info-box-text">{{translate_in($key)}}</span>
                                            <span class="info-box-number">{{$item}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                </a>
                            </div>
                            @endforeach
                            @endif
                            @endforeach
                        </div>
                        {{-- {!! $roles->render() !!} --}}

                    </div>
                </div>
                <!-- /.card -->

                <!-- card detail -->
                <div class="card" id="detail">
                    <div class="card-header">
                        <h3 class="card-title">Detail Cuti</h3>

                    </div>
                    <div class="card-body">
                        <div class="loader" style="display: none;"></div>
                        <div class="row">
                            <div class="col-12">
                                <div class="not-avail"></div>
                                <div class="timeline">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<!-- /.content -->
@endsection
@section('js-section')
<script src="https://js.pusher.com/8.2.0/pusher.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

<script>

    $('.date-paid-leave').datepicker({
        multidate: true,
        format: 'dd-mm-yyyy',
    });

    function detail_paid_leave(params) {
        $('.loader').show();

        $('.timeline').html('');
        var url = "{{route('paid-leaves.show', ':slug')}}";
        url = url.replace(':slug', params)
        $.get(url,
            function (data, textStatus, jqXHR) {
                if (data.length == 0) {
                    $('.loader').hide();
                    $('.timeline').html('Anda Belum Mengajukan Cuti');
                }
                var html = ``;
                for (let index = 0; index < data.length; index++) {
                    const element = data[index];
                    $('.timeline').html('');
                    const flag = element.flag == 'submit' ? 'primary' : element.flag == 'approved' ? 'success' : 'danger'
                    const flag_text = element.flag == 'submit' ? 'Mengajukan' : element.flag == 'approved' ? 'Disetujui' : 'Ditolak'
                    const button_flag_cuti = `<span class="badge badge-${flag}">${flag_text}</span>`
                    const button_download_surat_cuti = element.flag == 'approved' ? `<button onclick="event.preventDefault(); document.getElementById('${element.id}').submit();" class="btn btn-primary btn-sm">Download Surat Cuti</button>` : ''
                    const button_cancel_surat_cuti = element.flag == 'submit' ? `<span onclick="cancel('${element.id}')" style="float: right; margin: 3px" class="btn btn-danger btn-sm"><i class="fas fa-times"></i> Batalkan</span>` : '';
                    // var list_date_leave = `<ul>`
                    // for (let index = 0; index < element.lists_date_paid_leave.length; index++) {
                    //     const item = element.lists_date_paid_leave[index];
                    //     list_date_leave += `<li>${item}</li>`
                    // }
                    // list_date_leave += `</ul>`
                    html += `
                            <div class="time-label">
                              <span class="bg-${flag}">${element.submit_leave_day}</span>
                            </div>

                            <div>
                              <i class="fas fa-history bg-blue"></i>
                              <div class="timeline-item">
                                ${button_cancel_surat_cuti}
                                <h3 class="timeline-header">Status : ${button_flag_cuti}</h3>
                                <div></div>
                                <div class="timeline-body">
                                  ${element.explaination}
                                  <br>Banyak Cuti: <strong>${element.days} Hari</strong>
                                  <br>Tanggal Cuti: <strong>${element.lists_date_leave}</strong>
                                </div>
                                <div class="timeline-footer">
                                  ${button_download_surat_cuti}
                                    <form id="${element.id}" method="post" action="{{route('paid-leave.print')}}" style="display:none;">
                                        @csrf
                                        <input text="hidden" name="id" value="${element.id}"/>
                                    </form>
                                </div>
                              </div>
                            </div>`;
                    $('.loader').hide();

                    $('.timeline').html(html);
                }
            },
            "JSON"
        );
    }

    $('#leave_paid').on('change', function() {
        $("#form-submit-paid-leave input").prop("disabled", false);
        $.get("{{route('paid-leaves.create')}}", {'paid_leave_cat_id':this.value},
            function (data, textStatus, jqXHR) {
                if (data.remains == 0) {
                    alert('Cuti Sudah Habis')
                    $("#form-submit-paid-leave input").prop("disabled", true);
                }
                $("#remianing_paid_leave").html(`Sisa Cuti: ${data.remains}`);
                $('input[name=days]').attr('max', data.remains)
            },
            "JSON"
        );
    });

    Pusher.logToConsole = true;

    var pusher = new Pusher('d298640228c2f3ac76ff', {
    cluster: 'ap1'
    });

    var channel = pusher.subscribe('my-channel');
    channel.bind('my-event', function(data) {
        console.log(data);
        // $('#alert-paid-leave').show('slow');
    });

    function cancel(paid_leave_id) {
        var url = "{{route('paid-leaves.destroy', ':id')}}"
        url = url.replace(':id', paid_leave_id)
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            data: {'_method': 'delete'},
            url: url,
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    alert(response.success)
                    location.reload();
                }
            }
        });
    }


</script>

@endsection
