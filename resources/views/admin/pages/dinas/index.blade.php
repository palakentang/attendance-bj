@extends('layouts.app-lte')
@section('css-section')
    <link rel="stylesheet" href="{{ asset('v2/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('v2/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('v2/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
<!-- Select2 -->
  <link rel="stylesheet" href="{{asset('v2/plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('v2/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{asset('v2/plugins/daterangepicker/daterangepicker.css')}}">
  <style>
    tbody tr {
        position: relative;
    }
  </style>

@endsection

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @include('components.alert')
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Tambah Perjalanan untuk PJLP </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        {!! Form::open(array('route' => 'admin.dinas.store','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
                        <div class="form-group">
                            <strong>Nomor Surat Dinas:</strong>
                            {!! Form::text('name', null, array('placeholder' => 'Nomor Surat Dinas','class' => 'form-control', 'required' => 'required')) !!}
                        </div>
                        <div class="form-group">
                            <strong>Yang Ditugaskan:</strong>
                            {!! Form::select('user_id[]', \App\Models\User::Role('pjlp')->pluck('name', 'id')->toArray(), '', ['class' => 'form-control select2', 'multiple' => "multiple", 'data-placeholder'=>"Pilih User", 'required' => 'required']) !!}
                        </div>
                        <div class="form-group">
                            <strong>Kategori Dinas:</strong>
                            {!! Form::select('service_cat_id', ['' => 'Pilih Kategori Dinas']+\App\Models\ServicesCat::whereFlag('enabled')->pluck('name', 'id')->toArray(), '', ['class' => 'form-control', 'data-placeholder'=>"Pilih Kategori Dinas", 'required' => 'required']) !!}
                        </div>
                        @component('components.date-range', [
                            'label' => 'Tanggal Perjalanan Dinas',
                            'name' => 'service_date'
                        ])
                        @endcomponent
                        <div class="form-group">
                            <strong>Deskripsi Dinas:</strong>
                            {!! Form::textarea('service_description', null, array('placeholder' => 'Deskripsi Dinas','class' => 'form-control', 'rows' => 3, 'cols' => '2')) !!}
                        </div>
                        <div class="form-group">
                            <strong>Surat Dinas</strong>
                            {{Form::file('surat_dinas',['class'=>'form-control'])}}
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Simpan', ['class' => 'btn btn-block btn-success']) !!}
                        </div>

                        {!! Form::close() !!}
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List Perjalanan {{ $pageTitle }} PJLP</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table  id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nomor Perjalanan Dinas</th>
                                    <th>Detail Perjalanan Dinas</th>
                                    <th>Yang Ditugaskan</th>
                                    <th>Pemberi Tugas</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
                <!-- /.card -->

            </div>
        </div>
</section>
@section('js-section')
    <script src="{{ asset('v2/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{asset('v2/plugins/select2/js/select2.full.min.js')}}"></script>
    <!-- Moment -->
    <script src="{{asset('v2/plugins/moment/moment.min.js')}}"></script>
    <!-- date-range-picker -->
    <script src="{{asset('v2/plugins/daterangepicker/daterangepicker.js')}}"></script>

    <script>
        $(function () {
            //Select2
            $('.select2').select2()

            //Date range picker
            $('#daterangecustom').daterangepicker({
                opens: 'rigth',
                drops: 'up',
                locale: {
                    format: 'DD-MM-YYYY',
                    separator: " s/d "
                },function(start, end, label) {
                   console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
                }
            })

        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "{{ route('admin.dinas.lists') }}",
                "type": "POST",
                "data": function (data) {
                    data.search = $('input[type="search"]').val();
                }
            },
            "order": ['1', 'DESC'],
            "pageLength": 10,
            "searching": true,
            "aoColumns": [
                    {
                        data: null,
                        render: function(data, type, row) {
                            var url_edit = "{{route('admin.dinas.edit', ':slug')}}"
                            url_edit = url_edit.replace(':slug', data.id)
                            let today = new Date().toISOString().slice(0, 10)

                            var url_destroy = "{{route('admin.dinas.destroy', ':slug')}}"
                            url_destroy = url_destroy.replace(':slug', data.id)
                            return `<strong>${row.name}</strong><br>Dibuat : <br>${row.created_at_humans} </br> <img src="/uploads/dinas/${today}/${row.letter_of_assignment}" class="mailbox-attachment-icon has-img"> <div style="position:absolute; bottom: 0; margin-block-end: 10px;"><a class="btn btn-success" href="${url_edit}">Edit</a> <a class="btn btn-danger" href="${url_destroy}">Hapus</a></div> `
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, row) {
                            var html = `<table style="width:100%">
                                    <tr>
                                        <th>Kategori Dinas</th>
                                        <td>${row.category.name}</td>
                                    </tr>
                                    <tr>
                                        <th>Deskripsi Dinas</th>
                                        <td>${row.description_ellipsis}</td>
                                    </tr>

                                    <tr>
                                        <th>Tanggal Perjalanan Dinas</th>
                                        <td><strong>${row.tanggal_mulai_dinas}</strong> <br>Sampai Dengan<br> <strong>${row.tanggal_akhir_dinas}</strong></td>
                                    </tr>
                                    </table>`
                                    return html;
                        }
                    },

                    {
                        data: 'details',
                        render: function(data, type, row) {
                            var assign_for = `<table style="width:100%">`
                            data.forEach(element => {
                                if (element.flag == 'submit') {
                                    var flag = `<span class="badge badge-danger">Belum Diterima User PJLP</span>`
                                }else{
                                    var flag = `<span class="badge badge-success">Sudah Diterima User PJLP</span>`
                                }
                                assign_for += `<tr>`
                                    assign_for += `<th>${element.user.name}</th>`
                                    assign_for += `<td>${flag}</td>`
                                assign_for += `</tr>`
                            });
                            assign_for += `</table>`;
                            return assign_for;
                        }
                    },
                    {
                        data: 'commissioned_by.name',
                    },

            ],
            columnDefs: [{
                "defaultContent": "-",
                "targets": "_all"
            }]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    </script>

@endsection
@endsection
