@extends('layouts.app-lte')
@section('css-section')
<link rel="stylesheet" href="{{ asset('v2/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('v2/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('v2/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
<link rel="stylesheet" href="https://cdn.datatables.net/datetime/1.5.1/css/dataTables.dateTime.min.css">

<style>
    table#example1 {
        width: 100%;
    }

    .modal-body{
        height: 70v;
        overflow-y: auto;
    }
</style>
@endsection
@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @include('components.alert')
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{$pageTitle}} {{ucfirst(auth()->user()->name)}}</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 col-6">
                              <!-- small box -->
                              <div class="small-box bg-info">
                                <div class="inner">
                                  <h3>{{notification(auth()->user(), 'present') ?? 0}}</h3>

                                  <p>Jumlah Absen Hari Ini</p>
                                </div>
                                <div class="icon">
                                  <i class="ion ion-bag"></i>
                                </div>
                                <a href="{{route('admin.attendances.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                              </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                              <!-- small box -->
                              <div class="small-box bg-success">
                                <div class="inner">
                                  <h3>{{notification(auth()->user(), 'late')?? 0}}</h3>

                                  <p>Jumlah Absen Telat Hari Ini</p>
                                </div>
                                <div class="icon">
                                  <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{route('admin.attendances.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                              </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                              <!-- small box -->
                              <div class="small-box bg-warning">
                                <div class="inner">
                                  <h3>{{notification(auth()->user(), 'paid-leave') ?? 0}}</h3>

                                  <p>Pengajuan Cuti / Izin / Sakit Hari Ini</p>
                                </div>
                                <div class="icon">
                                  <i class="ion ion-person-add"></i>
                                </div>
                                <a href="{{route('admin.paid-leaves.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                              </div>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-3 col-6">
                              <!-- small box -->
                              <div class="small-box bg-danger">
                                <div class="inner">
                                  <h3>{{notification(auth()->user(), 'dinas')?? 0}}</h3>

                                  <p>Perjalanan Dinas</p>
                                </div>
                                <div class="icon">
                                  <i class="ion ion-pie-graph"></i>
                                </div>
                                <a href="{{route('admin.dinas.index')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                              </div>
                            </div>
                            <!-- ./col -->
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">

                    </div>
                    <!-- /.card-footer-->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{$pageTitle}} {{ucfirst(auth()->user()->name)}}</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table border="0" cellspacing="5" cellpadding="5">
                            <tbody>
                                <tr id="tr_month">
                                    <td>Pencarian Bulanan</td>
                                    <td>
                                        <select name="month" id="month" >
                                            <option value="0">Pilih Bulan</option>
                                            <option value="1">Januari</option>
                                            <option value="2">Februari</option>
                                            <option value="3">Maret</option>
                                            <option value="4">April</option>
                                            <option value="5">Mei</option>
                                            <option value="6">Juni</option>
                                            <option value="7">Juli</option>
                                            <option value="8">Agustus</option>
                                            <option value="9">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">November</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Pencarian Nama:</td>
                                    <td><input type="text" id="search" name="search"></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-md-12">

                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Nama User</th>
                                            <th>Cuti / Izin</th>
                                            <th>Sakit</th>
                                            <th>Terlambat / Pulang Cepat</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="modal fade" id="modal-default">
                            <div class="modal-dialog modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">List Presensi <span id="tanggal"><span></h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-header">

                                </div>
                                <div class="modal-body">
                                     <div class="card card-outline card-success">
                                        <div class="card-header">
                                          <h3 class="card-title">Riwayat Cuti Sakit</h3>


                                          <!-- /.card-tools -->
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">

                                        </div>

                                        <!-- /.card-body -->
                                      </div>

                                </div>
                                <div class="card-footer"></div>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                          <!-- /.modal -->

                        <div class="modal fade" id="modal-sick">
                            <div class="modal-dialog modal-lg modal-dialog-scrollable">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h4 class="modal-title">List Cuti Sakit <span id="tanggal"><span></h4>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                    <div class="timeline">

                                    </div>
                                </div>
                                <div class="card-footer"></div>
                              </div>
                              <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                          <!-- /.modal -->
                    </div>
                    <!-- /.card-body -->

                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
@section('js-section')
<script src="{{ asset('v2/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('v2/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('v2/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('v2/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('v2/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.2/moment.min.js"></script>
<script src="https://cdn.datatables.net/datetime/1.5.1/js/dataTables.dateTime.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var Datatables = $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": true,
            "processing": true,
            "serverSide": true,
            "searching": false,
            scrollY: 500,
            scroller: true,
            "ajax": {
                "url": "{{ route('admin.dashboard.lists') }}",
                "type": "POST",
                "data": function (data) {
                    data.search = $('#search').val();
                    data.month = $('#month').val();
                }
            },
            "pageLength": 10,
            "aoColumns": [
                    { // Nama
                        data: 'name',
                        name: 'name',
                    },
                    { // Cuti
                        data: 'paid_leaves',
                        render: function(data, type, row) {
                            // Menginisialisasi objek untuk menyimpan hasil perhitungan
                            let counts = {};

                            // Periksa apakah paid_leaves ada dan tidak kosong sebelum mengaksesnya
                            if (row.paid_leaves && row.paid_leaves.length > 0) {
                                // Menghitung jumlah setiap kategori_leave_id
                                row.paid_leaves.forEach(item => {
                                    let categoryName = item.category.name;
                                    counts[categoryName] = (counts[categoryName] || 0) + 1;
                                });
                            } else {
                                // Jika paid_leaves kosong, berikan pesan yang sesuai atau lakukan tindakan lain
                                return  'Belum mengajukan Cuti';
                            }

                            // Membangun tabel HTML baru untuk hasil perhitungan
                            let output = '<table>';
                            output += '<thead><tr><th>Jenis Cuti</th><th>Jumlah</th></tr></thead>';
                            output += '<tbody>';

                            // Menambahkan baris untuk setiap kategori
                            Object.entries(counts).forEach(([categoryName, count]) => {
                                output += `<tr><td>${categoryName}</td><td>${count}</td></tr>`;
                            });

                            output += '</tbody></table>';

                            return output;
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, row ) {
                            return `<a onclick="sick(${row.id})">Lihat Di sini</a>`
                        }
                    },
                    { // Terlambat
                        data: null,
                        render: function (data, type, row) {
                            if (row.attendances[0] != undefined) {
                                // console.log(row.attendances[0].attendance_date);
                                return `<a onclick="late_in(${row.id}, '${row.attendances[0].attendance_date}')">Lihat Di sini</a>`
                            }
                        }
                    },
            ],
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        function late_in(user_id, month) {
            $('#modal-default').modal('show')
            $('#tanggal').html(month)
            $.post("{{route('admin.attendances.lists.filter')}}", {user_id, month},
                function (data, textStatus, jqXHR) {
                    var html = `<div class="row">`
                    for (let index = 0; index < data.data.length; index++) {
                        const element = data.data[index];
                        if (element.attendance_in != null) {
                            const badge = element.attendance_in.is_late ? 'danger' : 'success'
                            html += `<div class="col-md-3">`
                            html += `<div class="card card-outline card-${badge}"><div class="card-header">`
                                html += `<h3 class="card-title">${element.attendance_date_human}</h3>`
                            html += ` </div>`
                            html += `<div class="card-body">Terlambat: ${element.is_late}<br>Pulang Cepat: ${element.is_go_early}</div>`
                            html += `</div></div>`
                        }
                    }
                    html += `</div>`
                    $('.modal-body').html(html);
                    $('#total_terlambat').html(data.limitLateTime);
                    if (data.limitLateTime < 1000) {
                        $('#total_terlambat').attr('class', 'badge badge-success');
                    }else if (data.limitLateTime > 1500 && data.limitLateTime <= 2300){
                        $('#total_terlambat').attr('class', 'badge badge-warning');
                    }else{
                        $('#total_terlambat').attr('class', 'badge badge-danger');
                    }
                },
                "json"
            );
        }

        function sick(user_id) {
            $('#modal-sick').modal('show')
            $('.timeline').html('');
            var url = "{{route('show.detail.module', ['user_id' => ':user_id', 'module' => ':module'])}}";
            url = url.replace(':user_id', user_id).replace(':module', 'sick');

            $.get(url,
                function (data, textStatus, jqXHR) {
                    var html = ``;
                    data.data.forEach(paid_leave => {
                        console.log(paid_leave);
                        var image = paid_leave.flag == 'approved' ? `<img src="${paid_leave.load_image}" alt="" class="img img-thumbnail">` : `` ;
                        var color_flag = paid_leave.flag == 'approved' ? `success"` : `primary` ;
                        html += `<div class="time-label">
                                <span class="bg-red" id="date_sick">${paid_leave.leave_date_humans}</span>
                            </div>
                            <div>
                                <i class="fas fa-envelope bg-blue"></i>
                                <div class="timeline-item">
                                <h3 class="timeline-header">Status: <span class="badge badge-${color_flag}">${paid_leave.flag_humans}</span></h3>

                                <div class="timeline-body">
                                    ${paid_leave.explaination}<br>
                                    ${image}
                                </div>
                                </div>
                            </div>`;
                    });
                    $('.timeline').append(html);
                },
                "JSON"
            );
        }

        function early_go(user_id, month) {
            console.log(user_id, month);
        }

        let table = new DataTable('#example1');

        document.querySelectorAll('#search').forEach((el) => {
            el.addEventListener('change', () => table.draw());
        })

        $('#month').on('change', function (e) {
            e.preventDefault();
            if ($('#month').find(":selected").val() != '0') {
                $('#min').prop('disabled', true);
            } else {
                $('#min').prop('disabled', false);
            }
            table.draw()

        });
</script>
@endsection
