@extends('layouts.app-lte')
@section('css-section')
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<style>
    .ui-timepicker-container {
    z-index: 99999 !important;
}
</style>

@endsection
@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @include('components.alert')
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <div class="pull-right">

                            </div>
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Key</th>
                                    <th>Value</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                <tr>
                                    <td>{{ucwords(str_replace('_', ' ', $item->key))}}</td>
                                    <td>
                                        <ul>
                                            @foreach ($item->values as $key => $value)
                                            <li>{{ucwords(str_replace('_', ' ', $key))}} : {{ucfirst(strtolower($value))}}</li>
                                            @endforeach
                                        </ul>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-success" data-toggle="modal"
                                            data-target="#{{$item->key}}">
                                            <i class="fas fa-edit"></i>
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>



        @component('admin.pages.settings.form.custom_clock', [
            'id' => 'custom_clock'
        ])

        @endcomponent


        @component('admin.pages.settings.form.office_hour', [
            'id' => 'office_hour'
        ])

        @endcomponent

    </div>

</section>
<!-- /.content -->
@endsection
@section('js-section')

@endsection
