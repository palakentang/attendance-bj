<div class="modal fade" id="{{$id}}">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ucwords(str_replace('_', ' ', $id))}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(array('route' => null,'method'=>'POST', 'enctype' =>
                'multipart/form-data')) !!}
                @method('PUT')
                {!! Form::hidden('key', null, ['id' => 'key']) !!}
                <div class="form-group">
                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                        <input name="active" type="checkbox" class="custom-control-input" id="customSwitch3">
                        <label class="custom-control-label" for="customSwitch3">Aktifkan</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nama Jam Khusus</label>
                    <input name="name" type="text" class="form-control" id="name">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Lama Jam Kerja</label>
                    <input name="many_hours_of_work" type="number" class="form-control" id="many_hours_of_work">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Maksimal Jam Masuk</label>
                    <input name="max_attendance_in" class="form-control" id="max_attendance_in">
                </div>
                {!! Form::submit('Simpan', ['class' => 'btn btn-success']) !!}
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
<script>
    $('#custom_clock').on('shown.bs.modal', function (e) {
        $('#customSwitch3').prop('checked', false);

        var url = "{{route('settings.show', ':id') }}";
        url = url.replace(":id", e.target.id);
        $.get(url,
            function (data, textStatus, jqXHR) {
                if (data.active) {
                    $('#customSwitch3').prop('checked', true);
                }
                $('#many_hours_of_work').val(data.many_hours_of_work);
                $('#max_attendance_in').val(data.max_attendance_in);
                $('#name').val(data.name);
                $('#key').val(e.target.id)
            },
            "json"
        );

        $('#max_attendance_in').timepicker({
            timeFormat: 'HH:mm',
            // year, month, day and seconds are not important
            minTime: new Date(0, 0, 0, 8, 0, 0),
            maxTime: new Date(0, 0, 0, 16, 0, 0),
            // time entries start being generated at 6AM but the plugin
            // shows only those within the [minTime, maxTime] interval
            startHour: 6,
            // the value of the first item in the dropdown, when the input
            // field is empty. This overrides the startHour and startMinute
            // options
            startTime: new Date(0, 0, 0, 8, 00, 0),
            // items in the dropdown are separated by at interval minutes
            interval: 30
        });

        var url_update = "{{route('settings.update', ':id')}}";
        url_update = url_update.replace(':id', e.target.id);

        var form = $('form');
        form.attr('action', url_update);

    })



</script>
