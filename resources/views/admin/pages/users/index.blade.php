@extends('layouts.app-lte')
@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        @include('components.alert')
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">
                            <div class="pull-right">
                                <a class="btn btn-success" href="{{ route('users.create') }}"> Create New User</a>
                                <a class="btn btn-success" onclick="importUser()"> Import User</a>
                            </div>
                        </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tr>
                                <th width="10px">No</th>
                                <th width="10px">Name</th>
                                <th width="10px">Email</th>
                                <th width="10px">Roles</th>
                                <th width="10px">Permissions</th>
                                <th width="280px">Action</th>
                            </tr>
                            @foreach ($data as $key => $user)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if (!empty($user->getRoleNames()))
                                    @foreach ($user->getRoleNames() as $v)
                                    <label class="badge badge-success">{{ $v }}</label>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    @if (!empty($user->getAllPermissions()))
                                    @foreach ($user->getAllPermissions() as $permission)
                                    <label class="badge badge-success">{{ $permission->name }}</label>
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('users.show', $user->id) }}">Show</a>
                                    <a class="btn btn-primary" href="{{ route('users.edit', $user->id) }}">Edit</a>
                                    {!! Form::open([
                                    'method' => 'DELETE',
                                    'route' => ['users.destroy', $user->id],
                                    'style' => 'display:inline',
                                    ]) !!}
                                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                    <button type="button" class="btn btn-success" onclick="addPermission({{$user}})">
                                        Give Permission
                                    </button>
                                </td>
                            </tr>
                            @endforeach
                        </table>


                        {!! $data->render() !!}
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        Footer
                    </div>
                    <!-- /.card-footer-->
                </div>
                <!-- /.card -->
            </div>
        </div>
        <div class="modal fade" id="modal-md">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Permission</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    {!! Form::open(array('route' => 'permissions.store','method'=>'POST')) !!}
                    <div class="modal-body">
                        <div class="form-group">
                            <strong>Name:</strong>
                            {!! Form::text('name', null, array('placeholder' => 'Name', 'id' => 'name', 'class' =>
                            'form-control', 'readonly')) !!}
                        </div>
                        <div class="form-group">
                            <strong>Permission:</strong>
                            <select class="form-control" name="permission_id[]" id="permissions" multiple>
                                @foreach ($permissions as $permission)
                                <option value="{{$permission->id}}">{{$permission->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <input class="form-control" type="hidden" name="user_id" id="user_id">

                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="import-modal">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Import User</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    {!! Form::open(array('route' => 'users.import','method'=>'POST', 'enctype' => 'multipart/form-data')) !!}
                    <div class="modal-body">
                        <div class="form-group">
                            <strong>File:</strong>
                            {!! Form::file('file_import', null, array('placeholder' => 'Name', 'id' => 'name', 'class' => '')) !!}
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

    </div>
</section>
<!-- /.content -->
@endsection
@section('js-section')
<script>
    function addPermission(user) {
        $('#permissions option:selected').removeAttr('selected');
        user.permissions.forEach(permission => {
            $('#permissions option[value=' + permission.id + ']').attr('selected', true);
        });
        $('#modal-md').modal('toggle');
        $('#user_id').val(user.id)
        $('#name').val(user.name)
    }

    function importUser() {
        $('#import-modal').modal('show')
    }
</script>
@endsection
