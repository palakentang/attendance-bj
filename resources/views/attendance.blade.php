@extends('layouts.app-lte')
@section('content')
<!-- Main content -->
<section class="content">


    <div class="container-fluid">
        @include('components.alert')
        <div class="row">
            <div class="col-md-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">{{date_humans(date('Y-m-d'))}} <span id="jam"></span> </h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{{route('attendance.store')}}" method="post">
                            <div class="row">
                                @csrf
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div id="my_camera"></div>
                                    <br />
                                    <center><input class="btn btn-success" type=button value="Take Snapshot"
                                            onClick="take_snapshot()"></center>
                                    <input type="hidden" name="image" class="image-tag">
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <center>
                                        <div id="results" style="height: 350px">Your captured image will appear here...</div>
                                        <div id="demo"></div>
                                    </center>
                                    <input type="hidden" name="attendance[lat]" id="lat">
                                    <input type="hidden" name="attendance[long]" id="long">
                                    <br><center><button class="btn btn-success">Simpan Absen</button></center>
                                </div>

                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                    </div>
                    <!-- /.card-footer-->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
@section('js-section')
<script>

    $(function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    });

    Webcam.set({
        height: 350,
        image_format: 'jpeg',
        jpeg_quality: 90
    });

    Webcam.attach( '#my_camera' );

    var shutter = new Audio();
    shutter.autoplay = false;
    shutter.src = navigator.userAgent.match(/Firefox/) ? '{{asset("v2/shutter.ogg")}}' : '{{asset("v2/shutter.mp3")}}';

    function take_snapshot() {
        shutter.play();

        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        } );

    }

    const x = document.getElementById("demo");

    function showPosition(position) {
        var currentdate = new Date();
        $('#lat').val(position.coords.latitude)
        $('#long').val(position.coords.longitude)
    }

</script>
@endsection
