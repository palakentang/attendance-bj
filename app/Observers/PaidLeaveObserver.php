<?php

namespace App\Observers;

use App\Events\CutiEvent;
use App\Models\PaidLeave;
use App\Models\Notifications;

class PaidLeaveObserver
{
    public const MODEL = 'App\Models\PaidLeave';
    public function creating(PaidLeave $paidLeave) {
        auth()->user()->tenant->tenant_name == 'yanip' ? $staff = 4 : $staff = 9;
        Notifications::create([
            'id' => \Str::uuid(),
            'notif_for' => $paidLeave->user_id,
            'notif_to' => $staff,
            'notif_module' => 'PaidLeave'
        ]);
    }
    /**
     * Handle the PaidLeave "created" event.
     *
     * @param  \App\Models\PaidLeave  $paidLeave
     * @return void
     */
    public function created(PaidLeave $paidLeave)
    {
        // $notifications = Notifications::whereUserId($paidLeave->user_id)->whereNotNull('readed_at')->get();
        event(new CutiEvent('success'));
    }

    /**
     * Handle the PaidLeave "updated" event.
     *
     * @param  \App\Models\PaidLeave  $paidLeave
     * @return void
     */
    public function updated(PaidLeave $paidLeave)
    {
        //
    }

    /**
     * Handle the PaidLeave "deleted" event.
     *
     * @param  \App\Models\PaidLeave  $paidLeave
     * @return void
     */
    public function deleted(PaidLeave $paidLeave)
    {
        //
    }

    /**
     * Handle the PaidLeave "restored" event.
     *
     * @param  \App\Models\PaidLeave  $paidLeave
     * @return void
     */
    public function restored(PaidLeave $paidLeave)
    {
        //
    }

    /**
     * Handle the PaidLeave "force deleted" event.
     *
     * @param  \App\Models\PaidLeave  $paidLeave
     * @return void
     */
    public function forceDeleted(PaidLeave $paidLeave)
    {
        //
    }
}
