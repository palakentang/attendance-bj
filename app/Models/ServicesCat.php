<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicesCat extends Model
{
    use HasFactory;
    protected $table = 'tb_service_cat';
    protected $guarded = [];
}
