<?php

namespace App\Models;

use App\Scopes\YanipScope;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    public const VALIDATION_RULES = [
        'email' => [
            'required',
            'unique:users'
        ]
    ];

    public const VALIDATION_MESSAGES = [
        'email.required' => 'Email Dibutuhkan',
        'email.unique:users' => 'Email Sudah Digunakan'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'tenant_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot() {

        parent::boot();
        if (\Auth::check()) {
            static::addGlobalScope(new YanipScope);
        }
    }

    /**
     * Get all of the PaidLeaves for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function PaidLeaves()
    {
        return $this->hasMany(PaidLeave::class);
    }

    /**
     * Get all of the PaidLeaves for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Sicks()
    {
        return $this->hasMany(PaidLeave::class)->where('category_leave_id', 2);
    }

    /**
     * Get all of the PaidLeaves for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function PaidLeave($date)
    {
        return $this->hasOne(PaidLeave::class)->whereDate('leave_date', $date);
    }

    /**
     * Get the Attendance associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Attendance(): HasOne
    {
        return $this->hasOne(Attendances::class);
    }

    /**
     * Get all of the Attendances for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Attendances(): HasMany
    {
        return $this->hasMany(Attendances::class);
    }

    /**
     * Get all of the Dinas for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Dinases(): HasMany
    {
        return $this->hasMany(ServicesDetail::class);
    }

    /**
     * Get the Tenant that owns the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tenant(): BelongsTo
    {
        return $this->belongsTo(Tenants::class);
    }

    public function scopeYanip($query) {
        return $query->where('tenants.tenant_name', 'yanip')->join('tenants', 'tenants.id', '=', 'users.tenant_id');
    }
    public function scopeBeritaJakarta($query) {
        return $query->whereTenantName('berita jakarta');
    }

}
