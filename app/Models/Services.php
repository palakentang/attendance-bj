<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Services extends Model
{
    use HasFactory;
    protected $table = 'tb_services';
    protected $guarded = [];
    protected $appends = [
        'tanggal_mulai_dinas',
        'tanggal_akhir_dinas',
        'description_ellipsis',
        'created_at_humans'
    ];
    public $incrementing = false;
    protected $primaryKey = 'id';
    public $keyType = 'string';


    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = \Str::uuid()->toString();
        });
    }
    function getCreatedAtHumansAttribute() {
        return date_humans(date('Y-m-d', strtotime($this->created_at)));
    }
    function getDescriptionEllipsisAttribute() {
        return ellipsis($this->service_description, 30);
    }
    public function getTanggalMulaiDinasAttribute() {
        return date_humans($this->service_date_start);
    }
    public function getTanggalAkhirDinasAttribute() {
        return date_humans($this->service_date_end);
    }

    /**
     * Get the CommisionedBy associated with the ServicesDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function CommissionedBy(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'commissioned_by');
    }

    /**
     * Get all of the Details for the Services
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Details(): HasMany
    {
        return $this->hasMany(ServicesDetail::class, 'service_id', 'id');
    }

    /**
     * Get the Category that owns the Services
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Category(): BelongsTo
    {
        return $this->belongsTo(ServicesCat::class, 'service_cat_id');
    }
}
