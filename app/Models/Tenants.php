<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tenants extends Model
{
    use HasFactory;
    protected $table = 'tenants';
    protected $guarded = [];

    /**
     * Get all of the Users for the Tenants
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Users(): HasMany
    {
        return $this->hasMany(User::class);
    }
}
