<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\ExportTrait;
use App\Traits\AttendanceTrait;
use Illuminate\Database\Eloquent\Model;
use App\Repository\Setting\SettingRepository;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Attendances extends Model
{
    use HasFactory, AttendanceTrait;
    protected $table = 'tb_attendances';
    public $incrementing = false;
    protected $primaryKey = 'id';
    public $keyType = 'string';
    protected $guarded = '';
    protected $casts = [
        'attendance_in' => 'array',
        'attendance_out' => 'array',
    ];

    protected $appends = [
        'is_late',
        'is_go_early',
        'is_flexi_time',
        'attendance_date_human',
        'name',
        'late_time',
        'working_hours',
        'less_working_hours'
    ];

    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = \Str::uuid()->toString();
        });
    }

    /**
     * Get the User that owns the Attendances
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    function getNameAttribute() {
        return User::find($this->user_id)->name;
    }

    function getLateTimeAttribute() {
        if (!empty($this->attendance_in['attendance_params'])) {
            return $this->attendance_in['is_late'] ? $this->timeLate(date('H:i', strtotime($this->attendance_in['attendance_time']))) : '-';
        } return '-';
    }

    public function getWorkingHoursAttribute() {
        if ($this->attendance_in && $this->attendance_out) {
            $start = Carbon::createFromFormat('H:i', date('H:i', strtotime($this->attendance_in['attendance_params']['time'])));
            $end = Carbon::createFromFormat('H:i', date('H:i', strtotime($this->attendance_out['attendance_params']['time'])));
            $diff = $end->diff($start);

            // Mendapatkan selisih waktu dalam jam dan menit
            $hours = $diff->h;
            $minutes = $diff->i;

            // Convert to minutes
            $totalMinutes = ($hours * 60) + $minutes;

            if ($totalMinutes < 420) {
                return 'Kurang :'.abs($totalMinutes - 420).' Menit';
            }

            return sprintf('%02d', $hours).':'.sprintf('%02d', $minutes);
        } return '-';
    }

    public function getLessWorkingHoursAttribute() {
        if ($this->attendance_in && $this->attendance_out) {
            $start = Carbon::createFromFormat('H:i', date('H:i', strtotime($this->attendance_in['attendance_params']['time'])));
            $end = Carbon::createFromFormat('H:i', date('H:i', strtotime($this->attendance_out['attendance_params']['time'])));
            $diff = $end->diff($start);

            // Mendapatkan selisih waktu dalam jam dan menit
            $hours = $diff->h;
            $minutes = $diff->i;

            // Convert to minutes
            $totalMinutes = ($hours * 60) + $minutes;
            if ($totalMinutes < 420) {
                return abs($totalMinutes - 420);
            } return '-';
        } return '-';
    }

    public function getIsLateAttribute() {
        if (!empty($this->attendance_in['attendance_params'])) {
            return $this->attendance_in['is_late'] ? '<span class="badge badge-danger">YA</span>' : '<span class="badge badge-success">TIDAK</span>';
        } return '-';
    }

    function getIsGoEarlyAttribute() {
        if (!empty($this->attendance_in)) {
            if (!empty($this->attendance_out['attendance_params'])) {
                $start = Carbon::createFromFormat('H:i', date('H:i', strtotime($this->attendance_in['attendance_params']['time']))) ?? Carbon::createFromFormat('H:i', date('Y-m-d'));
                $end = Carbon::createFromFormat('H:i', date('H:i', strtotime($this->attendance_out['attendance_params']['time'])));
                $diff = $end->diff($start);
                // Mendapatkan selisih waktu dalam jam dan menit
                $hours = $diff->h;
                $minutes = $diff->i;

                // Menghitung total menit
                $totalMinutes = ($hours * 60) + $minutes;

                // Menghitung total jam dalam format desimal
                $totalHours = $totalMinutes / 60;

                // Setting CustomClock
                $settingRepository = new SettingRepository();
                if ($settingRepository->getRamadhanClock()->active == 'NO') {
                    // Kondisi jika total jam kurang dari 8 jam
                    if ($totalHours < 8) {
                        return "<span class='badge badge-danger'>KURANG</span>";
                    } else if($totalHours > 8) {
                        return '<span class="badge badge-success">LEBIH</span>';
                    }else{
                        return '<span class="badge badge-success">CUKUP</span>';
                    }
                }else{

                    if ($totalHours < $settingRepository->getRamadhanClock()->many_hours_of_work) {
                        return "<span class='badge badge-danger'>KURANG</span>";
                    } else if($totalHours > $settingRepository->getRamadhanClock()->many_hours_of_work) {
                        return '<span class="badge badge-success">LEBIH</span>';
                    }else{
                        return '<span class="badge badge-success">CUKUP</span>';
                    }
                }
            } return '-';
        }
    }

    function getIsFlexiTimeAttribute() {
        if (!empty($this->attendance_in)) {
            $time_in = Carbon::createFromFormat('H:i', date('H:i', strtotime($this->attendance_in['attendance_params']['time'])));
            if ($time_in <= (Carbon::createFromFormat('H:i', '08:00'))) {
                return 'Tepat Waktu';
            }else if($time_in >  Carbon::createFromFormat('H:i', '08:00') && $time_in <= Carbon::createFromFormat('H:i', '09:30')){
                return 'Flexi Time';
            }else{
                return 'Terlambat';
            }
        }

    }

    function scopePresent($query, $date = null) {
        $date = $date ?? date('Y-m-d');
        return $query->whereNotNull('attendance_in')->whereAttendanceDate($date)->whereHas('user', function ($query) {
                $query->whereTenantId(auth()->user()->tenant->id);
        } );
    }

    function scopeLate($query, $date = null) {
        $date = $date ?? date('Y-m-d');
        return $query->whereTime('attendance_in->attendance_time', '>', '09.30')->whereAttendanceDate($date)->whereHas('user', function ($query) {
            $query->whereTenantId(auth()->user()->tenant->id);
        });
    }

    public function getAttendanceDateHumanAttribute() {
        return date_humans($this->attendance_date);
    }
}
