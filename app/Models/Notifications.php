<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Notifications extends Model
{
    use HasFactory;
    protected $table = 'notifications';
    protected $guarded = [];
    public $incrementing = false;
    protected $primaryKey = 'id';
    public $keyType = 'string';

}
