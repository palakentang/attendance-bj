<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    use HasFactory;
    protected $table = 'tb_settings';
    protected $guarded = [];
    protected $primaryKey = 'key';
    protected $increments = false;
    protected $casts = [
        'values' => 'object',
        'key' => 'string'
    ];
    protected $appends = [
        'extract_values'
    ];

    function scopeRamadhanOfficeHour($query) {
        return $query->find('ramadhan_office_hour')->values;
    }

    /**
     * Custom Clock
     */
    public function scopeCustomClock($query) {
        return $query->find('custom_clock')->values;
    }

    public function scopeWorkingHoursAllDays($query) {
        return $query->find('office_hours');
    }

    public function extractValuesAttribute($query, $key, $key_value) {

    }
}
