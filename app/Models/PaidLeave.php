<?php

namespace App\Models;

use Carbon\Carbon;
use App\Models\User;
use App\Services\HolidayServices;
use Illuminate\Support\Str;
use App\Traits\PaidLeaveTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PaidLeave extends Model
{
    use HasFactory, PaidLeaveTrait;
    protected $table = 'tb_paid_leave';
    public $incrementing = false;
    public $keyType = 'string';
    protected $guarded = ['id'];
    protected $appends = [
            'amount_of_leave_day',
            'submit_leave_day',
            'leave_date_humans',
            'range_date_humans',
            'lists_date_leave',
            'flag_humans',
            'load_image'
    ];

    public const PAID_LEAVE_TYPES = [
        1 => 'yearly' , // Tahunan
        5 => 'mourn' , // Duka Cita
        6 => 'caring' , // Merawat
        7 => 'married' , // Menikah
        8 => 'pregnant' , // Hamil
    ];

    public const FLAG = [
        'submit' => 'Mengajukan',
        'approved' => 'Disetujui',
        'declined' => 'Ditolak'
    ];

    public static function boot(){
        parent::boot();

        static::creating(function ($issue) {
            $issue->id = Str::uuid(36);
        });
    }

    /**
     * Get the Category associated with the PaidLeave
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Category(): HasOne
    {
        return $this->hasOne(PaidLeaveCat::class, 'id', 'category_leave_id');
    }

    public function User() {
        return $this->belongsTo(User::class);
    }

    public function scopeSubmit($query) {
        return $query->where('flag', 'submit');
    }

    function scopeSick($query) {
        return $query->whereFlag('submit')->whereCategoryLeaveId(2);
    }

    function scopeSicks($query) {
        return $query->whereCategoryLeaveId(2);
    }

    public function scopeApproved($query, $user_id = null) {
        return $user_id == null ? $query->whereFlag('approved') : $query->whereFlag('approved')->whereUserId($user_id);
    }
    public function scopeIsSickToday($query, $date = null) {
        $date == null ?? date('Y-m-d');
        return $query->whereCategoryLeaveId(2)->whereDate('leave_date', $date);
    }

    /**
     * Range Date Human
     *  for Paid Leave
     */
    public function getRangeDateHumansAttribute() {
        return $this->leave_date_humans.' sampai dengan '.$this->amount_of_leave_day;
    }

    function getFlagHumansAttribute() {
        return self::FLAG[$this->flag];
    }

    function getLoadImageAttribute() {
        return asset('uploads/paid_leave/sicks/'.$this->evidence);
    }

    function getListsDateLeaveAttribute() {
        return $this->extractDataPaidLeave($this->list_date_paid_leave);
    }

    public function getAmountOfLeaveDayAttribute() {
        if ($this->days == 1) {
            $day_leave = $this->days;
            $date = date_humans(date('Y-m-d', strtotime($this->leave_date)));
            return $date;
        } else{
            $day_leave = $this->days;
            $date = Carbon::createFromFormat('Y-m-d', date('Y-m-d', strtotime($this->leave_date)));
            return $this->calculateDayPaidLeave($date, $day_leave);
        }
    }

    /**
     * Convert Day to Date Humans
     * Leave Day
     */
    public function getSubmitLeaveDayAttribute() {
        return date_humans(date('Y-m-d', strtotime($this->created_at)));
    }

    /**
     * Convert Date to Date Humans
     * Leave Date
     */
    public function getLeaveDateHumansAttribute() {
        return date_humans(date('Y-m-d', strtotime($this->leave_date)));
    }

    public static function getPaidLeaveTenant($search = null, $month = null, $stats = null) {
        return self::whereHas('user', function ($query) use($search, $month, $stats) {
            $query->whereTenantId(auth()->user()->id);
            if ($month != null) {
                $query->orWhereMonth('leave_date', $month);
            }
            $query->orWhereMonth('leave_date', date('m'));
            if ($stats != null) {
                $query->Where('flag', $stats);
            }else{
                $query->Where('flag', 'submit');
            }
        } )->with(['Category', 'User'])->whereNotIn('category_leave_id', [2])->orderBy('leave_date');
    }

}
