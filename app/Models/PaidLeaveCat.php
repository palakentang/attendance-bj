<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaidLeaveCat extends Model
{
    use HasFactory;
    protected $table = 'tb_paid_leave_cat';
    protected $guarded = [];

}
