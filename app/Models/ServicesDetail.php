<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ServicesDetail extends Model
{
    use HasFactory;
    protected $table = 'tb_service_detail';
    protected $guarded = [];

    public function scopeSubmit($query, $user_id = null) {
        return $user_id == null ? $query->whereFlag('submit') : $query->whereFlag('submit')->whereUserId($user_id);
    }
    public function scopeApproved($query) {
        return $query->whereFlag('approved');
    }

    /**
     * Get the Services that owns the ServicesDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Services(): BelongsTo
    {
        return $this->belongsTo(Services::class, 'service_id', 'id');
    }

    /**
     * Get the PJLP associated with the ServicesDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function PJLP(): HasOne
    {
        return $this->hasOne(User::class);
    }

    /**
     * Get the User associated with the ServicesDetail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function User(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
