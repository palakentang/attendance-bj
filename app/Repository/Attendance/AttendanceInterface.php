<?php
namespace App\Repository\Attendance;

interface AttendanceInterface {
    public function create($user_id, $attendance_param, $date = null);
    public function update($user_id, $attendance_param);
    public function export($file);
    public function import($file);
    public function limitLateTime($user_id);
    public function listsAttendancesByUser($user_id, $month);

}
