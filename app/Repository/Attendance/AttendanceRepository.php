<?php

namespace App\Repository\Attendance;

use App\Models\Attendances;
use Illuminate\Support\Str;
use App\Traits\AttendanceTrait;
use Illuminate\Support\Facades\DB;

class AttendanceRepository implements AttendanceInterface
{
  use AttendanceTrait;
  public function create($user_id, $attendance_param, $date = null)
  {
    $date == null ? date('Y-m-d') : $date;
    try {
      DB::beginTransaction();
      Attendances::create([
        'id' => Str::uuid()->toString(),
        'user_id' => $user_id,
        'attendance_date' => date('Y-m-d'),
        'attendance_in' => $attendance_param
      ]);
      DB::commit();
      return true;
    } catch (\Throwable $th) {
      DB::rollBack();
      dd($th->getLine());
    }
  }

  public function update($user_id, $attendance_param)
  {
    try {
      DB::beginTransaction();
      Attendances::whereUserId($user_id)
        ->where('attendance_date', date('Y-m-d'))
        ->update($attendance_param);
      DB::commit();
      return true;
    } catch (\Throwable $th) {
      DB::rollBack();
      dd($th->getMessage());
      return false;
    }

  }

  function export($file)
  {
  }

  function import($file)
  {
  }

  function limitLateTime($user_id) {
    $late_times = Attendances::whereUserId($user_id)->whereMonth('attendance_date', date('m'))->pluck('attendance_in');
    $totalLateTime = collect($late_times)
            ->filter(function ($item) {
                // Filter element not null and has key 'late_time'
                return is_array($item) && array_key_exists('late_time', $item);
            })
            ->sum('late_time');
    return $totalLateTime;
  }

  function listsAttendancesByUser($user_id, $month) {
    $attendances['data'] = Attendances::whereUserId($user_id)->whereMonth('attendance_date', date('m', strtotime($month)))->orderBy('attendance_date', 'asc')->get();
    $attendances['limitLateTime'] = $this->limitLateTime($user_id);
    return $attendances;
  }
}
