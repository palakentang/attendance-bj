<?php
namespace App\Repository\Image;

interface ImageInterface {
    public function upload($file, $path, $filename);
    public function getFile();
    public function decodeB64();
}
