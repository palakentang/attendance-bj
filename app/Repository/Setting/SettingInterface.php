<?php
namespace App\Repository\Setting;

interface SettingInterface {
    function getRamadhanClock();
    function getOfficeHoursAllDays();
}
