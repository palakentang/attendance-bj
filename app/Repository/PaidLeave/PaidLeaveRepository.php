<?php
namespace App\Repository\PaidLeave;

use App\Events\CutiEvent;
use App\Models\PaidLeave;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use App\Repository\PaidLeave\PaidLeaveInterface;

class PaidLeaveRepository implements PaidLeaveInterface
{
    /**
     * Filter Paid Leave Based ON Category Paid Leave and User ID
     * @param int $category_id
     * @param int $user_id
     * @return PaidLeave
     */
    public function FilterPaidLeaveBasedONCategoryNUserID($category_id, $user_id) {
        // TODO: Implement FilterPaidLeaveBasedONCategoryNUserID() method
        return PaidLeave::whereCategoryLeaveId($category_id)->whereUserId($user_id)->orderBy('created_at', 'desc')->get();
    }

    public function create($request) {
        try {
            DB::beginTransaction();
            $newPaidLeave = new PaidLeave(); // Create a new instance of the PaidLeave model
            $newPaidLeave->id = Str::uuid(); // Assign a UUID to the id attribute
            $newPaidLeave->user_id = auth()->user()->id;
            $newPaidLeave->list_date_paid_leave = json_encode($request['list_date_paid_leave']);
            $newPaidLeave->days = $request['days'];
            $newPaidLeave->leave_date = $request['leave_date'];
            $newPaidLeave->category_leave_id = $request['category_leave_id'];
            $newPaidLeave->explaination = $request['explaination'];
            $newPaidLeave->save(); // Save the new record to the database
            DB::commit();
            return array('success' => true, 'message' => 'Berhasil');
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th->getMessage());
        }
    }
    public function approve($user_id, $category_leave_id, $leave_date) {
        try {
            DB::beginTransaction();
            $paidLeave = PaidLeave::where(['flag'=>'submit', 'user_id' => $user_id, 'category_leave_id' => $category_leave_id, 'leave_date' => $leave_date])->first();
            $paidLeave->flag = 'approved';
            $paidLeave->verified_by = $user_id;
            $paidLeave->save();
            DB::commit();
            return array('success' => true, 'message' => 'Berhasil Setuju Cuti');
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th->getMessage());
        }
    }
    public function decline($user_id, $category_leave_id, $leave_date) {
        try {
            DB::beginTransaction();
            $paidLeave = PaidLeave::where(['flag'=>'submit', 'user_id' => $user_id, 'category_leave_id' => $category_leave_id, 'leave_date' => $leave_date])->first();
            $paidLeave->flag = 'declined';
            $paidLeave->verified_by = $user_id;
            $paidLeave->save();
            DB::commit();
            return array('success' => true, 'message' => 'Berhasil Tolak Cuti');
        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th->getMessage());
        }
    }
    function show($paid_leave_id) {
        $paidLeave = PaidLeave::find($paid_leave_id);
        return $paidLeave;
    }
    function createSick($request){
        try {
            DB::beginTransaction();
            if (PaidLeave::whereLeaveDate(date('Y-m-d'))->whereUserId($request->user_id)->whereCategoryLeaveId($request->category_leave_id)->count() == 0) {
                $request['id'] = Str::uuid();
                self::create($request);
                DB::commit();
                return array('success' => true, 'message' => 'Berhasil');
            }else{
                return array('success' => false, 'message' => 'Gagal, Izin sudah diajukan');
            }

        } catch (\Throwable $th) {
            DB::rollBack();
            dd($th->getMessage());
        }

    }
}
