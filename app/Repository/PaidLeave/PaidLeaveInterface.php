<?php
namespace App\Repository\PaidLeave;

interface PaidLeaveInterface {
    function FilterPaidLeaveBasedONCategoryNUserID($category_id, $user_id);
    function create($request);
    function approve($user_id, $category_leave_id, $leave_date);
    function decline($user_id, $category_leave_id, $leave_date);
    function show($paid_leave_id);
    function createSick($request);
}
