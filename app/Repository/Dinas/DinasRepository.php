<?php
namespace App\Repository\Dinas;

use App\Models\Services;
use App\Models\ServicesDetail;
use Illuminate\Support\Facades\DB;
use App\Repository\Service\ServiceInterface;

class DinasRepository implements DinasInterface
{
  public function create($request) {
    try {
      DB::beginTransaction();
      $service = Services::create([
          'service_cat_id' => $request['service_cat_id'],
          'name' => $request['name'],
          'service_number' => $request['name'],
          'service_description' => $request['service_description'],
          'service_date_start' => date('Y-m-d', strtotime($request['date_start'])),
          'service_date_end' => date('Y-m-d', strtotime($request['date_end'])),
          'commissioned_by' => auth()->user()->id,
          'letter_of_assignment' => $request['letter_of_assignment']
      ]);

      for ($i=0; $i < count($request['user_id']); $i++) {
        $this->create_detail($service, $request['user_id'][$i]);
      }

      DB::commit();
      return $service;
    } catch (\Throwable $th) {
      DB::rollBack();
      \Log::error('Error: '.$th->getMessage());
      return false;
    }
  }
  public function update($request) {
    try {
      DB::beginTransaction();
      DB::commit();
    } catch (\Throwable $th) {
      DB::rollBack();
    }
  }
  public function delete($request) {
    try {
      DB::beginTransaction();
      DB::commit();
    } catch (\Throwable $th) {
      DB::rollBack();
    }
  }
  public function show($dinas_id) {
    return Services::findOrFail($dinas_id);
  }

  public function approve($dinas_id) {
    try {
        DB::beginTransaction();
        ServicesDetail::whereServiceId($dinas_id)->whereUserId(auth()->user()->id)->update(['flag' => 'approved']);
        DB::commit();
        return true;
    } catch (\Throwable $th) {
        DB::rollBack();
        \Log::error($th->getMessage());
        dd($th->getMessage());
    }

  }
  public function create_detail($service, $user_id) {
    ServicesDetail::create([
      'service_id' => $service->id,
      'user_id' => $user_id,
      'letter_of_assignment' => '-'
    ]);
  }
}
