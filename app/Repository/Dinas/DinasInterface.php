<?php
namespace App\Repository\Dinas;

interface DinasInterface {
  public function create($request);
  public function update($request);
  public function delete($request);
  public function show($request);
  public function create_detail($service, $request);
  public function approve($dinas_id);
}
