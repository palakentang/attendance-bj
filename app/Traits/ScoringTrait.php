<?php
namespace App\Traits;

use Carbon\Carbon;

trait ScoringTrait {

    function init($attendances, $flag) {
        switch ($flag) {
            case 'sumLate':
                $return = $this->sumLate($attendances);
                break;
            case 'sumLessWorkingHours':
                $return = $this->sumLessWorkingHours($attendances);
                break;
            case 'notAbsentInTheMorning':
                $return = $this->notAbsentInTheMorning($attendances);
                break;
            case 'notAbsentInTheAfternoon':
                $return = $this->notAbsentInTheAfternoon($attendances);
                break;
            case 'sumNotAbsent':
                $return = $this->sumNotAbsent($attendances);
                break;
            case 'sumOnTime':
                $return = $this->sumOnTime($attendances, 'onTime');
                break;
            case 'flexiTime':
                $return = $this->sumOnTime($attendances, 'flexTime');
                break;
            case 'timeReduction':
                $return = $this->timeReduction($attendances);
                break;
            case 'scoreDicipline':
                $return = $this->scoreDicipline($attendances);
                break;

            default:
                # code...
                break;
        }
        return $return;
    }



    /**
     * Calculate  the total number of late days in a month.
     * $attendances Attendances All Days
     */
    public function sumLate($attendances) {
        // Sum Total Telat
        $total = 0;

        // Extract
        foreach ($attendances as $filteredRecord) {
            if (!empty($filteredRecord->attendance_in)) {
                $total += $filteredRecord->attendance_in['late_time'];
            }
        }

        return $total;
    }

    /**
     * Calculate the less working hours per day.
     * $attendances Attendances All Days
     */
    public function sumLessWorkingHours($attendances) {
        $total = 0;
        $arr = [];

        foreach ($attendances as $attendance) {
            if ($attendance->less_working_hours != '-') {
                $total += $attendance->less_working_hours;
            }
        }
        return $total;
    }

    /**
     * Calculate the total number of Not Absent In The Morning in a month.
     * $attendances Attendances All Days
     */
    public function notAbsentInTheMorning($attendances) {
        // Counting Not Attendance in the Morning
        $counting = 0;

        foreach ($attendances as $attendance) {
            if ( $attendance->attendance_in == null) {$counting ++;}
        }
        return $counting;
    }

    /**
     * Calculate the total number of Not Absent In The Morning in a month.
     * $attendances Attendances All Days
     */
    public function notAbsentInTheAfternoon($attendances) {
        // Counting Not Attendance in the Morning
        $counting = 0;

        foreach ($attendances as $attendance) {
            if ( $attendance->attendance_out == null) {
                $counting ++;
            }
        }
        return $counting;
    }

    public function sumNotAbsent($attendances) {
        $total_not_absent = $this->notAbsentInTheMorning($attendances) + $this->notAbsentInTheMorning($attendances);
        return $total_not_absent *150;
    }

    public function sumOnTime($attendances, $flag) {
        $flexTime = 0;
        $onTime = 0;
        $late = 0;
        foreach ($attendances as $attendance) {

            if ($attendance->is_flexi_time == 'Flexi Time') {
                $flexTime ++;
            }

            if ($attendance->is_flexi_time == 'Tepat Waktu') {
                $onTime ++;
            }

            if ($attendance->is_flexi_time == 'Terlambat') {
                $late ++;
            }
        }
        $total['flexTime'] = $flexTime;
        $total['onTime'] = $onTime;
        $total['late'] = $late;
        return $total[$flag];
    }

    public function timeReduction($attendances) {
        $total = $this->sumLate($attendances) +
                $this->sumLessWorkingHours($attendances) +
                $this->notAbsentInTheMorning($attendances) +
                $this->notAbsentInTheAfternoon($attendances) +
                $this->sumNotAbsent($attendances);
        return $total;
    }

    function scoreDicipline($attendances){
        if ($this->timeReduction($attendances) < 300) {
            if ($this->timeReduction($attendances) == 0 && $this->sumOnTime($attendances, 'flexTime') < 10) {
                return $nilai = 95;
            }
            if (($this->timeReduction($attendances) >= 1 && $this->timeReduction($attendances) <= 20 && $this->sumOnTime($attendances, 'flexTime') <= 10) || ($this->timeReduction($attendances) == 0 && $this->sumOnTime($attendances, 'flexTime') >= 10)) {
                return $nilai = 90;
            }

            if ($this->timeReduction($attendances) >= 21 && $this->timeReduction($attendances) <= 100) {
                return $nilai = 85;
            }
            if ($this->timeReduction($attendances) >= 101 && $this->timeReduction($attendances) <= 300) {
                return $nilai = 80;
            }
            $nilai = 75;
        }else{
            $nilai = 75;
        }
        return $nilai;
    }

}
