<?php

namespace App\Traits;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


trait Uuids
{
    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($issue) {
            $issue->id = Str::uuid();
        });


    }

}
