<?php

namespace App\Traits;

use Carbon\Carbon;
use App\Models\User;
use App\Events\CutiEvent;
use App\Models\PaidLeave;
use App\Traits\PrintTrait;
use App\Models\PaidLeaveCat;
use App\Services\HolidayServices;
use Novay\WordTemplate\WordTemplate;
use App\Repository\PaidLeave\PaidLeaveRepository;

trait PaidLeaveTrait
{
    use UploadTrait, PrintTrait;
    public function calculatePaidLeave($users)
    {
        $resultArray = [];

        foreach ($users as $item) {
            $name = $item['name'];
            $totalLeavePaidYearly = 0;
            $totalLeavePaidMourn = 0;
            $totalLeavePaidCaring = 0;
            $totalLeavePaidMarried = 0;
            $totalLeavePaidPregnant = 0;

            foreach ($item['paid_leaves'] as $leave) {

                // Adjust the calculation based on category_leave_id
                // Assuming $leave['days'] is the number of days for each leave
                if ($leave['category_leave_id'] == 1) {
                    $totalLeavePaidYearly += $leave['days'];
                    $limitLeavePaidYearly = PaidLeaveCat::find($leave['category_leave_id'])->limit;
                }else if($leave['category_leave_id'] == 5) {
                    // Berduka
                    $totalLeavePaidMourn += $leave['days'];
                    $limitLeavePaidMourn = PaidLeaveCat::find($leave['category_leave_id'])->limit;
                }else if($leave['category_leave_id'] == 6) {
                    // Merawat
                    $totalLeavePaidCaring += $leave['days'];
                    $limitLeavePaidCaring = PaidLeaveCat::find($leave['category_leave_id'])->limit;
                }else if($leave['category_leave_id'] == 7) {
                    // Maried
                    $totalLeavePaidMarried += $leave['days'];
                    $limitLeavePaidMarried = PaidLeaveCat::find($leave['category_leave_id'])->limit;
                }else if($leave['category_leave_id'] == 8) {
                    // Pregnant
                    $totalLeavePaidPregnant += $leave['days'];
                    $limitLeavePaidPregnant = PaidLeaveCat::find($leave['category_leave_id'])->limit;
                }
            }

            $limitLeavePaidYearly = isset($limitLeavePaidYearly) ? $limitLeavePaidYearly : 12;
            $limitLeavePaidMourn = isset($limitLeavePaidMourn) ? $limitLeavePaidMourn : 3;
            $limitLeavePaidCaring = isset($limitLeavePaidCaring) ? $limitLeavePaidCaring : 3;
            $limitLeavePaidMarried = isset($limitLeavePaidMarried) ? $limitLeavePaidMarried : 3;
            $limitLeavePaidPregnant = isset($limitLeavePaidPregnant) ? $limitLeavePaidPregnant : 90;

            $resultArray[$name]['id'] = $item['id'] ;
            $resultArray[$name]['amount_of_leave_paid']['yearly'] = $limitLeavePaidYearly  - $totalLeavePaidYearly ;
            $resultArray[$name]['amount_of_leave_paid']['mourn'] = $limitLeavePaidMourn  - $totalLeavePaidMourn ;
            $resultArray[$name]['amount_of_leave_paid']['caring'] = $limitLeavePaidCaring  - $totalLeavePaidCaring ;
            $resultArray[$name]['amount_of_leave_paid']['married'] = $limitLeavePaidMarried  - $totalLeavePaidMarried ;
            $resultArray[$name]['amount_of_leave_paid']['pregnant'] = $limitLeavePaidPregnant - $totalLeavePaidPregnant;
        }
        return $resultArray;
    }

    public function isAmountPaidLeaveLeft($remains, $submit) {
        return $remains >= $submit;
    }

    public function updateStatus($request) {
        $paid_leave = new PaidLeaveRepository();
        if ($request->status == 'approve') {
            $update_stats = $paid_leave->approve($request->user_id, $request->category_leave_id, $request->leave_date);
            event(new CutiEvent('success'));
        } else {
            $update_stats = $paid_leave->decline($request->user_id, $request->category_leave_id, $request->leave_date);
            event(new CutiEvent('fail'));
        }
        return $update_stats;
    }

    function updateSickLeave($request) {
        $paid_leave = new PaidLeaveRepository();
        $request['category_leave_id'] = 2;
        $request['leave_date'] = $request['sick_date'];
        unset($request['sick_date']);
        $request['flag'] = 'approved';
        $request['user_id'] = auth()->user()->id;

        /** add Days */

        $date = Carbon::now();
        for ($i=1; $i < $request->days; $i++) {
            $list_date_paid_leave[] = $date->addDay()->format('d-m-Y');
        }
        $request['list_date_paid_leave'] = $list_date_paid_leave;
        $update_status = $paid_leave->createSick($request);
        return $update_status;
    }

    private function getLeaveLetter($id) {
        $paid_leave = new PaidLeaveRepository();
        $paid_leave_user = $paid_leave->show($id);

        $user = $this->calculatePaidLeave(User::whereId(auth()->user()->id)->with('PaidLeaves')->get()->toArray());
        $amount_of_leave_paid = $user[auth()->user()->name]['amount_of_leave_paid'];

        $yearly = $amount_of_leave_paid['yearly'];
        $mourn = $amount_of_leave_paid['mourn'];
        $caring = $amount_of_leave_paid['caring'];
        $pregnant = $amount_of_leave_paid['pregnant'];

        if ($paid_leave_user['flag'] == 'approved') {
            $range_date = $this->extractDataPaidLeave($paid_leave_user->list_date_paid_leave);

            $tipe_cuti = $paid_leave_user->Category->name;
            $tanggal_isi = date_humans(date('Y-m-d', strtotime($paid_leave_user->created_at)));
            $nama = $paid_leave_user->User->name;
            $lama_cuti = $paid_leave_user->days;
            $terbilang = ucfirst(terbilang($lama_cuti));
            $alasan = $paid_leave_user->explaination;

            $file = public_path('static/cuti/draft_cuti.rtf');

            $array = array(
                'KIW' => $range_date,
                'TIPE_CUTI' => $tipe_cuti,
                'TANGGAL_ISI' => $tanggal_isi,
                'NAMA' => $nama,
                'LAMA_CUTI' => $lama_cuti.' ('.$terbilang.')',
                '[ALASAN_CUTI]' => $alasan,
                'YEARLY' => $yearly
            );
            $nama_file = 'surat_cuti-'.$nama.'.doc';
            // $word = new WordTemplate;

            $document = $this->export($file, $array, $nama_file);
            return $document;
        }
    }

    function calculateDayPaidLeave($date_submit, $amount_days) {
        $holidayService = new HolidayServices();
        $getNextDay = $holidayService->getNextDay($date_submit, $amount_days);
        $date = date_humans(date('Y-m-d', strtotime($getNextDay)));
        return $date;
    }

    /**
     * @param string
     * @return @string
     */
    function extractDataPaidLeave($lists) {
        $lists = json_decode($lists);
        $explodedDates = '';
        if (count($lists) == 1) {
            return date_humans(date('Y-m-d', strtotime($lists[0])));
        }else if (count($lists) == 2) {
            $return = '';
            foreach ($lists as $item) {
                $return .= date_humans(date('Y-m-d', strtotime($item))).' & ';
            }
            return rtrim($return, ' & ');
        }else{
            foreach ($lists as $key => $date) {
                if ($key < count($lists) - 1) {
                    $explodedDates .= date_humans(date('Y-m-d', strtotime($date))) . ', ';
                } else {
                    // Karakter pemisah terakhir
                    $lastSeparator = ' & ';
                    $explodedDates .= $lastSeparator.date_humans(date('Y-m-d', strtotime($date)));
                }
            }

            $explodedDates = rtrim($explodedDates, ', ');

            return $explodedDates;
        }
    }
}
