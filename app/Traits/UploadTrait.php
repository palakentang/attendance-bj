<?php
namespace App\Traits;

trait UploadTrait
{
    function uploadSuratDinas($path, $file) {
        if(!\File::exists($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        $fileName = \Str::slug(auth()->user()->name).'-'.\Str::random(5).time().'.'.$file->getClientOriginalExtension();
        $file->move(public_path($path), $fileName);
        return $fileName;
    }

    function uploadSuratSakit($user, $path, $file) {
        if(!\File::exists($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        $fileName = \Str::slug($user).'-'.\Str::random(5).time().'.'.$file->getClientOriginalExtension();
        $file->move(public_path($path), $fileName);
        return $fileName;
    }

    /**
     * Process Upload
     */
    function uploadFile($path, $imageName, $file) {
        if(!\File::exists(public_path($path))) {
            \File::makeDirectory(public_path($path), 0777, true);
        }
        \File::put(public_path($path.'/') .$imageName, $file);
        return array('path' => $path, 'filename' => $imageName);
    }

    /**
     * Process Upload From Base64
     */
    function uploadBase64($request) {
        $file = $this->decodeBase64($request);
        return $this->uploadFile($file['path'], $file['image_name'], base64_decode($file['image']));
    }

    /**
     * Process Decode Base 64 File Usually Image
     */
    function decodeBase64($request) {
        $image = $request['image'];  // your base64 encoded
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = date('Y-m-d').'-'.\Str::random(5).'.jpeg';
        $path = '/uploads/absensi/'.\Str::slug(auth()->user()->name);
        return array('image_name' => $imageName, 'path' => $path, 'image' => $image);
    }
}
