<?php
namespace App\Traits;

use Carbon\Carbon;
use App\Traits\AttendanceTrait;
use Stevebauman\Location\Facades\Location;
use App\Repository\Attendance\AttendanceRepository;


trait DinasTrait {
    use AttendanceTrait;
    static $CATEGORY_TYPE = [
        'DINAS_LUAR_AWAL' => 1,
    ];

    function checkCategoryDinasID($dinas_type, $time) {
        $attendanceRepository = new AttendanceRepository;
        $date_range = $this->rangeDate($time['start'], $time['end']);
        switch ($dinas_type) {
            case self::$CATEGORY_TYPE['DINAS_LUAR_AWAL']:
                foreach ($date_range as $date) {
                    $attendanceRepository->create(
                        auth()->user()->id,
                        [
                            'attendance_params' =>
                            array(
                                'lat' => "-6.1814803",
                                "long" => "106.8265606",
                                "time" => "08.00",
                                "foto" => "-",
                            ),
                            'attendance_time' => '08.00',
                            'late_time' => 0,
                            'is_late' => false
                        ], $date
                    );
                }
                break;

            default:
                # code...
                break;
        }
    }

    private function rangeDate($start, $end) {
        $start = Carbon::parse($start);
        $end = Carbon::parse($end);

        $daysPassed = [];

        // Loop melalui setiap hari antara startDate dan endDate
        while ($start->lessThanOrEqualTo($end)) {
            $daysPassed[] = $start->toDateString();
            $start->addDay();
        }
        return $daysPassed;
    }
}
