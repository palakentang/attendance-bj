<?php
namespace App\Traits;

use Carbon\Carbon;
use App\Models\User;
use Carbon\CarbonInterval;
use App\Models\Attendances;
use App\Repository\Attendance\AttendanceRepository;
use App\Repository\Setting\SettingRepository;

trait AttendanceTrait
{
    static $NOT_TIME_TO_GO = 'absent_not_time_to_go';
    static $HAS_BEEN_OUT = 'absent_has_been_out';

    public function hasBeenAbsent($user_id, $request) {
        $attendance = Attendances::whereUserId($user_id)->where('attendance_date', date('Y-m-d'))->first();
        $attendanceRepository = new AttendanceRepository();

        $settingRepository = new SettingRepository();

        // Show Limit Time User
        $late_time = $attendanceRepository->limitLateTime($user_id);

        // Check if Late Time GT Limit Late Time In?
        if ($late_time >= config('constant.limit_late_time_in')) {
            return array('status' => 'fail', 'message' => 'Gagal Absen Masuk, Anda Sudah Telat '.CarbonInterval::minutes($late_time));
        }else{
            // is Late?
            $late_time = $this->timeLate();
            if ($attendance == null) {

                // Absen Masuk
                $attendanceRepository->create($user_id, [

                        'attendance_params' => $request['attendance'],
                        'attendance_time' => date('H:i'),
                        'late_time' => $late_time,
                        'is_late' => $late_time != 0
                ]);

                return array('status' => 'success', 'message' => 'Berhasil Absen Masuk');
            }

            // has Been First Absent?
            if ($attendance->attendance_in == null) {
                $late_time = $this->timeLate();

                $attendanceRepository->update($user_id, [
                    'attendance_in' => array(
                        'attendance_params' => $request['attendance'],
                        'attendance_time' => date('H:i'),
                        'late_time' => $late_time,
                        'is_late' => $late_time != 0
                    ),
                ]);
                return array('status' => 'success', 'message' => 'Berhasil Absen Masuk, Tapi Anda Telat '.CarbonInterval::minutes($late_time)->cascade()->forHumans());
            }else{
                // Go HOME!
                if($attendance->attendance_out == null){
                    if($settingRepository->getRamadhanClock()->active == 'YES') {
                        if($this->timeRangeValid($attendance->attendance_in['attendance_time'], $settingRepository->getRamadhanClock()->many_hours_of_work)) {
                            $attendanceRepository->update($user_id, ['attendance_out' => array('attendance_params' => $request['attendance'], 'attendance_time' => date('H:i')),]);
                            return array('status' => 'success', 'message' => 'Berhasil Absen Pulang');
                        } return array('status' => self::$NOT_TIME_TO_GO, 'message' => str_replace('{many_hours_of_work}', $settingRepository->getRamadhanClock()->many_hours_of_work, config('constant.error.'.self::$NOT_TIME_TO_GO)));
                    }

                    if($this->timeRangeValid($attendance->attendance_in['attendance_time'])) {
                        $attendanceRepository->update($user_id, ['attendance_out' => array('attendance_params' => $request['attendance'], 'attendance_time' => date('H:i')),]);
                        return array('status' => 'success', 'message' => 'Berhasil Absen Pulang');
                    } return array('status' => self::$NOT_TIME_TO_GO, 'message' =>  str_replace('{many_hours_of_work}', 8, config('constant.error.'.self::$NOT_TIME_TO_GO)));

                } return array('status' => self::$HAS_BEEN_OUT, 'message' =>  str_replace('{many_hours_of_work}', 8, config('constant.error.'.self::$HAS_BEEN_OUT)));
            }
        }

    }

    /**
     * Is Range Time 8 Hours?
     */
    function timeRangeValid($time_in, $many_hours_of_work = 8) {
        $time_in = Carbon::createFromFormat('H:i', $time_in);
        $time_out = Carbon::createFromFormat('H:i', date('H:i'));

        $duration = $time_in->diffInHours($time_out);
        if ($duration >= $many_hours_of_work) return true;
    }

    /**
     * Check Time Late
     * @return int $late_time
     */
    function timeLate($time_in = null) {
        $late_time_max = Carbon::createFromFormat('H:i', date('H:i', strtotime('09:30')));
        if ($time_in == null) {
            $time_in = Carbon::now();
        }else{
            $time_in = Carbon::createFromFormat('H:i', date('H:i', strtotime($time_in)));
        }
        $now_time = $time_in;
        if ($now_time->gt($late_time_max)) {
            return $now_time->diffInMinutes($late_time_max);
        } return 0;
    }

    /**
     * Is User Mobile Attendance?
     *  @return boolean
     */
    function isUserMobileAttendanceStatic($user_id) {
                                    // Eva,  Fahri,  Habib,  Rofi,  Fathul,  Aga
        return in_array($user_id, [202205, 202301, 202209, 202211, 202302, 202002]);
    }

    /**
     * Is record File excel any same date form last file?
     * @return boolean
     */
    function alreadyImportByDate($user_id, $date) {
        $importedData = Attendances::whereDate('attendance_date',$date)->whereUserId($user_id)->count();
        return $importedData > 0;
    }

    /**
     * Calculating Working Hours
     */
    function workingHours() {

    }

}
