<?php

// Di dalam class wrapper Anda
namespace App\Services;

use Carbon\Carbon;
use Grei\TanggalMerah;


class HolidayServices
{
    
    public function getNextDay($date, $amount_days)
    {
      return $this->checkWeekEnd($date, $amount_days);
    }

    function checkWeekEnd($date_submit, $amount_days) {
      $date = $date_submit;
      if ($date->addDay()->isWeekend()) {
        // Bring Day to Sunday
        $next_day = $date->addDay();        
        $next_day = $next_day->addDays($amount_days - 1);
      }else{
        $next_day = $date_submit->addDays($amount_days - 2); // ??????????????
        if ($next_day->dayOfWeek === Carbon::SATURDAY) {
          $next_day->addDays(2);
        } else if($next_day->dayOfWeek === Carbon::SUNDAY){
          $next_day->addDays(2);
        }else{
          $next_day;
          $this->checkHoliday($next_day, $amount_days);
        }
      }
      return $next_day;
    }

    function checkHoliday($day, $amount_days)  {
      $t = new TanggalMerah('static/holidays.json');
      $t->set_date($day);
      if ($t->is_holiday()) {
        $day->addDay();

        // Recursive For Check Weekend and Check Holiday
        return $this->checkWeekEnd($day, $amount_days);
      } return $day;
    }
}