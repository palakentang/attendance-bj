<?php

namespace App\Imports;

use App\Traits\AttendanceTrait;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Attendances;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AttendancesImport implements ToModel, WithHeadingRow
{
    use AttendanceTrait;
    private $rows = 0;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $date = Carbon::createFromFormat('d/m/Y', $row['date'])->format('Y-m-d');
        if ($date == '1970-01-01') {return;}


        if(!$this->isUserMobileAttendanceStatic($row['ac_no']) && !$this->alreadyImportByDate($row['ac_no'], $date)) {

            $user = User::find($row['ac_no']);
            $late_time = $this->timeLate($row['clock_in']);

            if (!empty($user)) {
                if ($row['clock_in'] != null) {
                    $attendance_in = array(
                        "attendance_time" => $row['clock_in'],
                        "attendance_params" => array(
                            "lat" => "-6.1814803",
                            "long" => "106.8265606",
                            "time" => $row['clock_in'],
                            "foto" => "-",
                        ),
                        'is_late' => $late_time != 0,
                        'late_time' => $late_time
                    );
                }else {
                    $attendance_in = null;
                }

                if ($row['clock_out'] != null) {
                    $attendance_out = array(
                        "attendance_time" => $row['clock_out'],
                        "attendance_params" => array(
                            "lat" => "-6.1814803",
                            "long" => "106.8265606",
                            "time" => $row['clock_out'],
                            "foto" => "-",
                        )
                    );
                }else{
                    $attendance_out = null;
                }

                $attendance = Attendances::where(['user_id' => $user->id, 'attendance_date' => $date]);
                if ($attendance->count() == 0) {
                    ++$this->rows;
                    return new Attendances([
                        'id' => Str::uuid()->toString(),
                        'user_id' => $user->id,
                        'attendance_date' => $date,
                        'attendance_in' => $attendance_in,
                        'attendance_out' => $attendance_out,
                    ]);
                }
            }else{
                dd($row['name']);
            }
        } return Attendances::whereDate('attendance_date', $date)->first();
    }

    public function headingRow(): int {
        return 1;
    }

    public function getRowCount(): int
    {
        return $this->rows;
    }
}
