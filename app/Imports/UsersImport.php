<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Validators\Failure;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Faker\Factory as Faker;


class UsersImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $faker = Faker::create('id_ID');
        if (auth()->user()->tenant->tenant_name != 'yanip') {
            $row['name'] = $faker->name;
            $row['tenant_id'] = 2;
        }else{
            $row['name'] = $row['nama'];
            $row['tenant_id'] = 1;
        }

        $this->import($row);
    }

    function import($row) {
        if (empty(User::find($row['ac_no']))) {
            $user = User::create([
                'id' => (int)$row['ac_no'],
                'nip' => $row['nip'],
                'name' => $row['name'],
                'email' => Str::slug($row['name']).'@mail.com',
                'password' => Hash::make('secret'),
                'tenant_id' => $row['tenant_id'],
            ]);
            if (in_array(Str::slug($row['nama']).'@mail.com', ['aga-prasetya@mail.com', 'eva-nurfatimah@mail.com', 'habib-akbar-aziiz@mail.com', 'muhammad-rofi-arofah@mail.com', 'muhammad-fahrizal@mail.com', 'fathul-hudoyo@mail.com'])) {
                $user->assignRole([5]);
            }
            $user->givePermissionTo(
                [
                    'leave-list','leave-create',
                    'dinas-list','dinas-create',
                    'attendance-list','attendance-create','dinas-edit','dinas-delete',
                ]
            );
            $user->assignRole([4]);
        }
    }

    public function headingRow(): int {
        return 2;
    }
}
