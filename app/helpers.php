<?php

use App\Models\Attendances;
use App\Models\PaidLeave;
use App\Models\Services;
use App\Models\ServicesDetail;

if(!function_exists('date_humans')) {
    function date_humans($date) {
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $date);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}

if (!function_exists('ellipsis')) {
    function ellipsis($string, $length) {
        return strlen($string) > $length ? substr($string,0,$length)."..." : $string;
    }
}

if(!function_exists('terbilang')) {
	function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim(penyebut($nilai));
		} else {
			$hasil = trim(penyebut($nilai));
		}
		return $hasil;
	}
}

if (!function_exists('penyebut')) {
    function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
		}
		return $temp;
	}
}

if (!function_exists('notification')) {
	function notification($user, $module) {
		if ($module == 'paid-leave') {
			$user->hasRole('staff') || $user->hasRole('superadmin') ? $return = PaidLeave::getPaidLeaveTenant()->count() : $return = PaidLeave::Approved($user->id)->count();
		}	else if($module == 'dinas'){
			$user->hasRole('staff') || $user->hasRole('superadmin') ? $return = ServicesDetail::Submit()->count() : $return = ServicesDetail::Submit($user->id)->count();
		} else if($module == 'present'){
			$user->hasRole('staff') || $user->hasRole('superadmin') ? $return = Attendances::Present()->count() : $return = 0;
		} else if($module == 'sick'){
			$user->hasRole('staff') || $user->hasRole('superadmin') ? $return = PaidLeave::Sick()->count() : $return = 0;
		}else{
			$user->hasRole('staff') || $user->hasRole('superadmin') ? $return = Attendances::Late()->count() : $return = 0;
		}

		if ($return == 0) {
			return;
		} return $return;
	}
}

if (!function_exists('counting')) {
    function counting() {
        $late_times = Attendances::whereUserId(auth()->user()->id)->whereMonth('attendance_date', date('m'))->pluck('attendance_in');
        $totalLateTime = collect($late_times)
                ->filter(function ($item) {
                    // Filter element not null and has key 'late_time'
                    return is_array($item) && array_key_exists('late_time', $item);
                })
                ->sum('late_time');
        return $totalLateTime;
    }
}

if (!function_exists('getMonth')) {
    function getMonth($month = null) {
        $months = [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        ];
        $month == null ? $return =  $months : $return = $months[$month];
        return $return;
    }
}

if(!function_exists('translate_in')) {
    function translate_in($string) {
        $translate = [
            'yearly' => 'Tahunan', //
            'mourn' => 'Duka Cita', //
            'caring' => 'Merawat', //
            'married' => 'Menikah', //
            'pregnant' => 'Hamil' , //
        ];
        return $translate[$string];
    }
}
