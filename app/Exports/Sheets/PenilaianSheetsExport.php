<?php
namespace App\Exports\Sheets;

use App\Models\User;
use App\Models\Attendances;
use App\Traits\ScoringTrait;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;

class PenilaianSheetsExport implements FromCollection, WithHeadings, WithMapping, WithTitle, WithEvents
{
    use RegistersEventListeners, ScoringTrait;
    private $month;
    private $year;

    public function __construct(int $year, int $month)
    {
        $this->month = $month;
        $this->year  = $year;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $month = $this->month;
        $user = User::with(['Attendances' => function ($query) use($month) {
            $query->whereMonth('attendance_date', $month);
            $query->orderBy('attendance_date', 'desc');
        }])->role('pjlp')->whereNotIn('name', ['pjlp', 'pjlp-2', 'pjlp-3', 'pjlp-4'])->orderBy('name')->get();
        return $user;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Penilaian ' .getMonth($this->month).' '.date('Y');
    }

    /**
     * Add Header
     */
    public function headings(): array
    {
        return [
            "Nama Pegawai", // A
            "Terlambat (Menit)", //B
            "Kurang Waktu Kerja (Menit)", //C
            'Total Tidak Absen Pagi', //D
            'Total Tidak Absen Sore', //E
            'Pengurangan Waktu', //F
            'Total Masuk Tepat Waktu', //G
            'Total Masuk Flexi', //H
            'Nilai Kedisplinan', //I
            'Tanggung Jawab Penyelesaian Pekerjaan', // J
            'Kepatuhan Terhadap Kewajiban dan Larangan', // K
            'Keterangan', //L
            'Total Menit Pengurangan Tidak Absen'
        ];
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function map($user): array
    {
        return [
            $user->name, // A
            $this->init($user->Attendances, 'sumLate') > 0 ?
                $this->init($user->Attendances, 'sumLate') : '0', // B

            $this->init($user->Attendances, 'sumLessWorkingHours') > 0 ?
                $this->init($user->Attendances, 'sumLessWorkingHours') : '0', // C

            $this->init($user->Attendances, 'notAbsentInTheMorning') > 0 ?
                $this->init($user->Attendances, 'notAbsentInTheMorning') : '0', // D

            $this->init($user->Attendances, 'notAbsentInTheAfternoon') > 0 ?
                $this->init($user->Attendances, 'notAbsentInTheAfternoon') : '0', // E

            $this->init($user->Attendances, 'timeReduction') > 0 ?
                $this->init($user->Attendances, 'timeReduction') : '0', // F

            $this->init($user->Attendances, 'sumOnTime') > 0 ?
                $this->init($user->Attendances, 'sumOnTime') : '0', // G

            $this->init($user->Attendances, 'flexiTime') > 0 ?
                $this->init($user->Attendances, 'flexiTime') : '0', // H

            $this->init($user->Attendances, 'scoreDicipline'), // I

            // '0', // I
            '0', // J
            '0', // K
            '', // L,
            $this->init($user->Attendances, 'sumNotAbsent') > 0 ? $this->init($user->Attendances, 'sumNotAbsent') : '0', // M
        ];
    }

    // public static function AfterSheet(AfterSheet $event)
    // {
    //     // $event->sheet returns \Maatwebsite\Excel\Sheet which has all the methods of \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
    //     $newRow = 2 + 1;

    //     $event->sheet->setCellValue('F'.$newRow, "=B.$+");
    //     $event->sheet->setCellValue('C'.$newRow, "=1+2");
    //     $event->sheet->setCellValue('D'.$newRow, "=1+3");
    //     $event->sheet->setCellValue('E'.$newRow, 'Totals');
    //     $event->sheet->setCellValue('F'.$newRow, "=1+1");
    //     $event->sheet->setCellValue('G'.$newRow, "=1+2");
    //     $event->sheet->setCellValue('H'.$newRow, "=1+3");
    // }
}

