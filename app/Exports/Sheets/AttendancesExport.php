<?php
namespace App\Exports\Sheets;

use App\Models\Attendances;
use App\Models\PaidLeave;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class AttendancesExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    private $month;
    private $year;

    public function __construct(int $year, int $month)
    {
        $this->month = $month;
        $this->year  = $year;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Attendances::whereHas('user', function($query)  {
        })
        ->whereMonth('attendance_date', $this->month)
        ->whereYear('attendance_date', $this->year)
        ->orderBy('attendance_date')->get();
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Absensi ' . getMonth($this->month).' '.date('Y');
    }

    /**
     * Add Header
     */
    public function headings(): array
    {
        return [
            "Nama",
            "Tanggal",
            "Jam Masuk",
            'Jam Pulang',
            'Jumlah Menit Telat',
            'Jumlah Jam Kerja',
            'Keterangan Keterlambatan',
            'Keterangan Jam Kerja',
            'Keterangan Flexi Time',
            'Keterangan Absensi Masuk',
            'Keterangan Absensi Pulang',
        ];
    }

    /**
     * @param Attendances $attendance
     *
     * @return array
     */
    public function map($attendance): array
    {
        $paid_leave = PaidLeave::whereUserId($attendance->user_id)->whereLike('list_date_paid_leave', date('d-m-Y', strtotime($attendance->attendance_date)))->first();
        if (!empty($paid_leave)) {
            return [
                $attendance->name,
                $attendance->attendance_date_human,
                $paid_leave->Category->name,
                $paid_leave->Category->name,
                $paid_leave->Category->name,
                $paid_leave->Category->name,
                $paid_leave->Category->name,
                $paid_leave->Category->name,
                $paid_leave->Category->name,
                $paid_leave->Category->name,
                $paid_leave->Category->name,
            ];
        }else{
            return [
                $attendance->name, // Nama
                $attendance->attendance_date_human, // Tanggal
                $attendance->attendance_in == null ? '-' : date('H:i', strtotime($attendance->attendance_in['attendance_time'])), // Jam Masuk
                $attendance->attendance_out == null ? '-' : date('H:i', strtotime($attendance->attendance_out['attendance_time'])), // Jam Pulang
                $attendance->late_time, // Jumlah Menit Telat

                $attendance->working_hours, // Jumlah Jam Kerja
                strip_tags($attendance->is_late) == 'YA' ? 'Terlambat' : 'Tidak Terlambat', // Keterangan Keterlambatan
                strip_tags($attendance->is_go_early), // Keterangan Jam Kerja
                $attendance->is_flexi_time, // Keterangan Flexi Time
                $attendance->attendance_in != null ? 'OK' : 'Belum Absen Masuk', // Keterangan Absensi Masuk
                $attendance->attendance_out != null ? 'OK' : 'Belum Absen Pulang', // Keterangan Absensi Pulang
            ];
        }

    }
}
