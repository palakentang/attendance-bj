<?php

namespace App\Exports;

use App\Models\User;
use App\Models\Attendances;
use App\Traits\ExportTrait;
use App\Exports\Sheets\AttendancesExport;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Exports\Sheets\PenilaianSheetsExport;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class AttendancesXPenilaianExport implements FromCollection, WithHeadings, WithMapping, WithMultipleSheets
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

    }

    /**
     * Add Header
     */
    public function headings(): array
    {
        return [
            "Nama",
            "Tanggal",
            "Jam Masuk",
            'Jam Pulang',
            'Jumlah Menit Telat',
            'Jumlah Jam Kerja',
            'Keterangan Keterlambatan',
            'Keterangan Jam Kerja',
            'Keterangan Flexi Time',
            'Keterangan Absensi Masuk',
            'Keterangan Absensi Pulang',
        ];
    }

    /**
     * @param Attendances $attendance
     *
     * @return array
     */
    public function map($attendance): array
    {
        return [
            $attendance->name, // Nama
            $attendance->attendance_date_human, // Tanggal
            $attendance->attendance_in == null ? '-' : date('H:i', strtotime($attendance->attendance_in['attendance_time'])), // Jam Masuk
            $attendance->attendance_out == null ? '-' : date('H:i', strtotime($attendance->attendance_out['attendance_time'])), // Jam Pulang
            $attendance->late_time == '-' ? 0 : $attendance->late_time, // Jumlah Menit Telat
            $attendance->working_hours, // Jumlah Jam Kerja
            strip_tags($attendance->is_late) == 'YA' ? 'Terlambat' : 'Tidak Terlambat', // Keterangan Keterlambatan
            strip_tags($attendance->is_go_early), // Keterangan Jam Kerja
            $attendance->is_flexi_time, // Keterangan Flexi Time
            $attendance->attendance_in != null ? 'OK' : 'Belum Absen Masuk', // Keterangan Absensi Masuk
            $attendance->attendance_out != null ? 'OK' : 'Belum Absen Pulang', // Keterangan Absensi Pulang

        ];
    }

    public function sheets(): array
    {
        return [
            new PenilaianSheetsExport(date('Y'), request()->month),
            new AttendancesExport(date('Y'), request()->month)
        ];
    }
}
