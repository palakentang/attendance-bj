<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\User;
use App\Models\PaidLeave;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use App\Models\ServicesDetail;
use App\Traits\PaidLeaveTrait;
use App\Http\Controllers\Controller;
use App\Repository\PaidLeave\PaidLeaveRepository;

class SickLeaveController extends Controller
{
    use PaidLeaveTrait, UploadTrait;
    protected $paidLeave;
    public function __construct(PaidLeaveRepository $paidLeave) {
        $this->paidLeave = $paidLeave;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sicks = PaidLeave::where('category_leave_id', 2)->submit()->get();
        return view('staff.pages.paidleaves.sick', [
            'sicks' => $sicks,
            'pageTitle' => 'Cuti Sakit'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $paid_leave = PaidLeave::findOrFail($request->sick_leave_id);
        try {
            $path = '/uploads/paid_leave/sicks/';
            $evidance = $this->uploadSuratSakit($paid_leave->User->name, $path, $request->file('evidance'));
            $paid_leave->leave_date = $request->fix_date;
            $paid_leave->flag = $request->flag == 'setuju' ? 'approved' : 'declined';
            $paid_leave->verified_by = auth()->user()->id;
            $paid_leave->evidence = $evidance;
            $fix_date = Carbon::parse($request->fix_date);
            for ($i=1; $i < $paid_leave->days; $i++) {
                $list_date_paid_leave[] = $fix_date->addDay()->format('d-m-Y');
            }
            $paid_leave->list_date_paid_leave = json_encode($list_date_paid_leave);
            $paid_leave->save();
            return redirect()->back()->with(['success' => 'Berhasil Update Sakit']);
        } catch (\Throwable $th) {
            dd($th->getMessage());
            return redirect()->back()->with(['fail' => 'Berhasil Update Sakit']);
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->calculatePaidLeave(User::find($id)->role(['pjlp'])->with(['PaidLeaves' => function($query) use($id) {
            return $query->whereUserId($id)->get()->toArray();
        }])->get()->toArray());
        return $user['pjlp'];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $cat_id = array_search($request->paid_leave_name, PaidLeave::PAID_LEAVE_TYPES);
        return $this->paidLeave->FilterPaidLeaveBasedONCategoryNUserID($cat_id, (int)$request->user_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function lists(Request $request) {
        // Page Length
        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;

        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';


        // Search
        //  get data from Paid Leave table
        $search = $request->search;
        $month = $request->month;
        $status = $request->status;
        $query = PaidLeave::getPaidLeaveTenant($search, $month, $status);

        // $orderByName = 'name';
        // switch($orderColumnIndex){
        //     case '0':
        //         $orderByName = 'name';
        //         break;
        //     case '1':
        //         $orderByName = 'email';
        //         break;
        // }
        // $query = $query->orderBy($orderByName, $orderBy);
        $recordsFiltered = $recordsTotal = $query->count();
        $users = $query->skip($skip)->take($pageLength)->get();

        return response()->json(["draw"=> $request->draw, "recordsTotal"=> $recordsTotal, "recordsFiltered" => $recordsFiltered, 'data' => $users], 200);
    }

    function lists_users() {
        $users = $this->calculatePaidLeave(User::role(['pjlp'])->whereNotIn('id', [5, 6, 7, 8, 9])->with(['PaidLeaves' => function($query){
            return $query->whereFlag('approved')->get()->toArray();
        }])->orderBy('name')->get()->toArray());
        return view('staff.pages.paidleaves.users', [
            'pageTitle' => 'Cuti Users',
            'users' => $users
        ]);

    }

    public function showByCatIDnUserID($cat_id, $user_id) {
        $data = $this->paidLeave->FilterPaidLeaveBasedONCategoryNUserID($cat_id, $user_id);
        return $data;
    }

}
