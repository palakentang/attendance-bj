<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PaidLeave;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.dashboard',
            ['pageTitle' => 'Dashboard']
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user_id
     * @param  string  $module
     * @return \Illuminate\Http\Response
     */
    public function show($user_id, $module)
    {
        if ($module == 'sick') {
            $data = PaidLeave::sicks()->whereUserId($user_id)->get();
            return ['status' => 'success', 'data' => $data];
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function lists(Request $request) {
        // Page Length
        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;

        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';


        // Search
        //  get data from products table
        $search = $request->search;
        $month = $request->month == 0 ? date('m') : $request->month;

        $query = User::role(['pjlp'])
            ->whereNotIn('name', ['pjlp', 'pjlp-2', 'pjlp-3', 'pjlp-4'])
            ->orderBy('name', 'asc');

        if (!empty($search)) {
            $query->where('name', 'like', '%'.$search.'%');
        }

        $recordsTotal = $query->count();
        $users = $query->skip($skip)->take($pageLength)->get();

        // Eager load PaidLeaves for the specified month
        $users->load(['PaidLeaves' => function($query) use ($month) {
            $query->with('Category')->whereMonth('leave_date', $month)->whereNotIn('category_leave_id', [2]);
        }]);

        // Eager load PaidLeaves for the specified month
        $users->load(['Sicks' => function($query) use ($month) {
            $query->whereMonth('leave_date', $month);
        }]);

        // Eager load Attendances for the specified month
        $users->load(['Attendances' => function ($query) use($month) {
            return $query->whereMonth('attendance_date', $month)->orderBy('attendance_date')->pluck('attendance_date');
        }]);

        $recordsFiltered = $recordsTotal;

        return response()->json([
            "draw" => $request->draw,
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsFiltered,
            'data' => $users->toArray()
        ], 200);
    }
}
