<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Attendances;
use App\Traits\ExportTrait;
use Illuminate\Http\Request;
use App\Exports\AttendancesExport;
use App\Imports\AttendancesImport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MultipleSheetsExport;
use App\Exports\AttendancesXPenilaianExport;
use App\Repository\Attendance\AttendanceRepository;

class AttendancesController extends Controller
{
    protected $attendancesRepository;
    public function __construct(AttendanceRepository $attendancesRepository) {
        $this->attendancesRepository = $attendancesRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('staff.pages.attendances.index', [
            'pageTitle' => 'Manajemen Absensi'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $request->month == null ? date('m') : $request->month;
        $attendances = $this->attendancesRepository->listsAttendancesByUser($request->user_id, $request->month);
        return $attendances;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function lists(Request $request) {

        // Page Length
        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;

        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';


        // Search
        //  get data from products table
        $search = $request->search;
        $date = $request->min;
        $month = $request->month;
        $query = Attendances::whereHas('user', function($query) use($search) {
            if ($search != null) {
                $query->whereTenantId(auth()->user()->id);
                $query->orWhere('name', 'like', "%".$search."%");
            }
        })
        ->with(['user.PaidLeaves' => function($query) use ($date) {
            $query->isSickToday($date)->first();
        }, 'user.roles' => function ($query) {
            $query->select('name');
        }]);


        if ($month != 0) {
            // Filter By Month
            $query->whereMonth('attendance_date', $month);
        }else{
            // Filter By Date
            if ($date != null) {
                $query->whereAttendanceDate(date('Y-m-d', strtotime($date)));
            }else{
                $query->whereAttendanceDate(date('Y-m-d'));
            }
        }
        $query->orderBy('attendance_date', 'asc');

        // $month != 0 ? $query->whereMonth('attendance_date', $month) :

        $recordsFiltered = $recordsTotal = $query->count();
        $users = $query->skip($skip)->take($pageLength)->get();

        return response()->json(["draw"=> $request->draw, "recordsTotal"=> $recordsTotal, "recordsFiltered" => $recordsFiltered, 'data' => $users], 200);
    }

    public function import(Request $request) {
        // dd($request);
        $import = new AttendancesImport;
        Excel::import($import, $request->file('file_import'));
        if ($import->getRowCount() > 0) {
            return redirect('admin/attendances')->with(['success' => 'Berhasil Import Data Absen']);
        } return redirect('admin/attendances')->with(['fail' => 'Gagal Import Data Absen, Ada Duplikasi Data!']);
    }

    public function export(Request $request) {
        // $this->calculateLate('09:31');
        // echo ($request->attendance_in == null ? '-' : $this->calculateLate(date('H:i', strtotime($request->attendance_in['attendance_time']))).'<br>');
        return Excel::download(new AttendancesXPenilaianExport, date_humans(date('Y-m-d')).'.xlsx');
    }
}
