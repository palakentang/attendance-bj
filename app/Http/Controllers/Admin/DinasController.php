<?php

namespace App\Http\Controllers\Admin;

use App\Models\Dinas;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Services;
use App\Repository\Dinas\DinasRepository;

class DinasController extends Controller
{
    use UploadTrait;
    protected $dinas;
    public function __construct(DinasRepository $dinasRepository) {
        $this->dinas = $dinasRepository;
    }
        /**
         * Display a listing of the resource.
         *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'dinas';
        return view('admin.pages.dinas.index', [
            'pageTitle' => $pageTitle
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        list($date_start, $date_end) = explode(' s/d ', $request->service_date);
        $request['date_start'] = $date_start;
        $request['date_end'] = $date_end;

        try {
            $path = 'uploads/dinas/'.date('Y-m-d').'/';
            $surat_dinas = $this->uploadSuratDinas($path, $request->file('surat_dinas'));
            $request['letter_of_assignment'] = $surat_dinas;
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }

        $dinas = $this->dinas->create($request->all());

        if ($dinas) {
            return redirect('/admin/dinas')->with(['success' => 'Berhasil Input Perjalanan Dinas']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dinas  $dinas
     * @return \Illuminate\Http\Response
     */
    public function show(Dinas $dinas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dinas  $dinas
     * @return \Illuminate\Http\Response
     */
    public function edit(Dinas $dinas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dinas  $dinas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dinas $dinas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dinas  $dinas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dinas $dinas)
    {
        //
    }

    public function lists(Request $request) {
        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;

        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';


        // Search
        //  get data from products table
        $search = $request->search;
        $query = Services::where('service_description', 'like', "%".$search."%")
                    ->orWhere('name', 'like', "%".$search."%")
                    ->orWhere('service_number', 'like', "%".$search."%")
                    ->orWhere('service_date_start', 'like', "%".$search."%")
                    ->orWhere('service_date_end', 'like', "%".$search."%")
                    ->with(['Details.User' => function($query) use($search) {
                        $query->orWhere('name', 'like', "%".$search."%")->get();
                    }, 'Category' => function($query) use ($search) {
                        $query->orWhere('name', 'like', "%".$search."%")->get();
                    }, 'CommissionedBy' => function($query) use($search) {
                        $query->orWhere('name', 'like', "%".$search."%")->get();
                    }]);

        // $orderByName = 'name';
        // switch($orderColumnIndex){
        //     case '0':
        //         $orderByName = 'name';
        //         break;
        //     case '1':
        //         $orderByName = 'email';
        //         break;
        // }
        // $query = $query->orderBy($orderByName, $orderBy);
        $recordsFiltered = $recordsTotal = $query->count();
        $data = $query->skip($skip)->take($pageLength)->get();

        return response()->json(["draw"=> $request->draw, "recordsTotal"=> $recordsTotal, "recordsFiltered" => $recordsFiltered, 'data' => $data], 200);
    }
}
