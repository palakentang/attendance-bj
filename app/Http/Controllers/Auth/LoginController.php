<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Scopes\YanipScope;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use Authenticatable;
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('auth.login');
    }

    public function store(LoginRequest $request)
    {
        $credentials = $request->except(['_token']);
        $user = User::withoutGlobalScope(YanipScope::class)->where('email', $credentials['email']);

        if (auth()->attempt($credentials)) {
            if (auth()->user()->hasRole('superadmin') || auth()->user()->hasRole('staff') ) {
                return redirect()->route('staff.dashboard');
            }else{
                return redirect()->route('dashboard');
            }
        }else{
            session()->flash('error', 'Invalid Credential');
            return redirect()->back();
        }
    }

    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
            return redirect()->route('auth.login.index')->with('success', 'Success Logout');
        } return redirect()->route('auth.login.index')->with('success', 'Success Logout');
    }
}
