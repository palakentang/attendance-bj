<?php

namespace App\Http\Controllers;

use App\Models\Services;
use App\Models\ServicesDetail;
use App\Repository\Dinas\DinasRepository;
use App\Traits\DinasTrait;
use Illuminate\Http\Request;

class DinasController extends Controller
{
    use DinasTrait;
    protected $dinas;
    public function __construct(
        DinasRepository $dinasRepository
        ) {
        $this->dinas = $dinasRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'dinas';

        $services = Services::whereHas('Details', function ($query)  {
            $query->where('user_id', auth()->user()->id);
        })->get();
        return view('dinas', [
            'pageTitle' => $pageTitle,
            'data' => $services
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dinas = $this->dinas->show($request->dinas_id);
        // dd($dinas);
        $this->checkCategoryDinasID($dinas->service_cat_id, ['start' => $dinas->service_date_start, 'end' => $dinas->service_date_end]);
        if($this->dinas->approve($request->dinas_id)) {
            return redirect()->back()->with(['success' => 'Berhasil Terima Dinas']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function show(Services $services)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function edit(Services $services)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Services $services)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function destroy(Services $services)
    {
        //
    }
}
