<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\PaidLeave;
use Illuminate\Http\Request;
use App\Traits\PaidLeaveTrait;
use Novay\WordTemplate\WordTemplate;
use Illuminate\Support\Facades\Artisan;
use App\Repository\PaidLeave\PaidLeaveRepository;

class PaidLeavesController extends Controller
{
    use PaidLeaveTrait;

    protected $paidLeave;
    public function __construct(PaidLeaveRepository $paidLeave) {
        $this->paidLeave = $paidLeave;
    }
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $users = $this->calculatePaidLeave(User::role(['pjlp'])->with(['PaidLeaves' => function($query){
            return $query->whereFlag('approved')->get()->toArray();
        }])->get()->toArray());

        return view('admin.pages.paidleaves.index', [
            'pageTitle' => 'Cuti',
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(Request $request)
    {
        $calculatePaidLeave = $this->calculatePaidLeave(User::role(['pjlp'])->with(['PaidLeaves' => function($query){
            return $query->whereFlag('approved')->get()->toArray();
        }])->get()->toArray());
        return array('remains' => $calculatePaidLeave[auth()->user()->name]['amount_of_leave_paid'][PaidLeave::PAID_LEAVE_TYPES[$request->paid_leave_cat_id]]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->leave_date == null) {
            redirect('/paid-leaves')->with(['fail' => 'gagal Mengajukan Cuti. Mohon Tanggal Diisi']);
        }
        if ($request->explaination == null) {
            redirect('/paid-leaves')->with(['fail' => 'gagal Mengajukan Cuti. Mohon Deskripsi Diisi']);
        }
        $request['list_date_paid_leave'] = explode(',', $request->leave_date);
        $request['days'] = count($request->list_date_paid_leave);
        $request['leave_date'] = date('Y-m-d');
        if ($request->sick_date) {
            $submit_sick = $this->updateSickLeave($request);
            return redirect()->back()->with(['success' => $submit_sick['message']]);
        }else{
            if ($request->days > 0) {
                $calculatePaidLeave = $this->calculatePaidLeave(User::whereId(auth()->user()->id)->with('PaidLeaves')->get()->toArray());
                if ($this->isAmountPaidLeaveLeft($calculatePaidLeave[auth()->user()->name]['amount_of_leave_paid'][PaidLeave::PAID_LEAVE_TYPES[$request->category_leave_id]], $request->days)) {
                    if($this->paidLeave->create($request->all())['success']) {
                        return redirect('/paid-leaves')->with(['success' => 'Berhasil Mengajukan Cuti']);
                    }
                }
                return redirect('/paid-leaves')->with(['fail' => 'gagal Mengajukan Cuti. Tidak ada sisa Cuti']);
            } return redirect('/paid-leaves')->with(['fail' => 'gagal Mengajukan Cuti. Jumlah Cuti  Melebihi Batas Minimum']);
        }
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        $data = $this->paidLeave->FilterPaidLeaveBasedONCategoryNUserID($id, auth()->user()->id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(PaidLeave $paidLeave)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cutis  $cutis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaidLeave $paidLeave)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return array
     */
    public function destroy($paid_leave_id)
    {
        if (PaidLeave::findOrFail($paid_leave_id)->delete()) {
            return ['success' => 'Cuti Berhasil Dibatalkan'];
        } return ['fail' => 'Cuti Gagal Dibatalkan'];
    }

    public function print(Request $request) {
        Artisan::call('cache:clear');
        return $this->getLeaveLetter($request->id);
    }

    public function sick(Request $request) {
        $update = $request->sick_date ? $this->updateSickLeave($request) : $this->updateStatus($request);
        return redirect()->back()->with(['success' => 'Berhasil Mengajukan Sakit']);
    }
}
