<?php

namespace App\Http\Controllers;

use App\Models\CutiKategori;
use Illuminate\Http\Request;

class CutiKategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CutiKategori  $cutiKategori
     * @return \Illuminate\Http\Response
     */
    public function show(CutiKategori $cutiKategori)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CutiKategori  $cutiKategori
     * @return \Illuminate\Http\Response
     */
    public function edit(CutiKategori $cutiKategori)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CutiKategori  $cutiKategori
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CutiKategori $cutiKategori)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CutiKategori  $cutiKategori
     * @return \Illuminate\Http\Response
     */
    public function destroy(CutiKategori $cutiKategori)
    {
        //
    }
}
