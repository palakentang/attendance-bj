<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use App\Models\tb_settings;
use Illuminate\Http\Request;
use Response;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.settings.index', [
            'pageTitle' => 'Settings',
            'data' => Settings::all()
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $setting_key
     * @return JSON
     */
    public function show($setting_key)
    {
        return Response::json(Settings::find($setting_key)->values);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\tb_settings  $tb_settings
     * @return \Illuminate\Http\Response
     */
    public function edit(tb_settings $tb_settings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\tb_settings  $tb_settings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $active = $request->active == 'on' ? 'YES' : 'NO';
        $settings = new Settings;
        $setting = $settings->find($request->key);
        $setting->update([
            'values' => [
                'name' => $request->name,
                'active' => $active,
                'max_attendance_in' => $request->max_attendance_in,
                'many_hours_of_work' => $request->many_hours_of_work
            ]
        ]);
        return redirect('/settings')->with(['success' => 'Berhasil Edit '.$request->key]);
        // {"name": "ramadhan", "active": true, "max_attendance_in": "08:30", "many_hours_of_work": 7}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\tb_settings  $tb_settings
     * @return \Illuminate\Http\Response
     */
    public function destroy(tb_settings $tb_settings)
    {
        //
    }
}
