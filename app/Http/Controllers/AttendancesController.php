<?php

namespace App\Http\Controllers;

use App\Models\Attendances;
use App\Repository\Attendance\AttendanceRepository;
use App\Traits\AttendanceTrait;
use App\Traits\UploadTrait;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Illuminate\Http\Response;

class AttendancesController extends Controller
{
    use UploadTrait, AttendanceTrait;
    protected $attendance;

    public function __construct(AttendanceRepository $attendance) {
        $this->attendance = $attendance;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('attendance',
            ['pageTitle' => 'Absent']
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request = $request->all();

      $request['attendance']['time'] = date('H:i');
      $file = $this->uploadBase64($request);
      $request['attendance']['foto'] = $file['path'].$file['filename'];
      $hasBeenAbsent = $this->hasBeenAbsent(auth()->user()->id, $request);
      if($hasBeenAbsent['status'] != 'success') {
        return redirect('/attendance')->with(['fail' => $hasBeenAbsent['message']]);
      } return redirect('/attendance')->with(['success' => $hasBeenAbsent['message']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Attendances  $attendances
     * @return \Illuminate\Http\Response
     */
    public function show(Attendances $attendances)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Attendances  $attendances
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendances $attendances)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Attendances  $attendances
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendances $attendances)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Attendances  $attendances
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendances $attendances)
    {
        //
    }


    public function lists(Request $request) {

        // Page Length
        $pageNumber = ( $request->start / $request->length )+1;
        $pageLength = $request->length;
        $skip       = ($pageNumber-1) * $pageLength;

        // Page Order
        $orderColumnIndex = $request->order[0]['column'] ?? '0';
        $orderBy = $request->order[0]['dir'] ?? 'desc';


        // Search
        //  get data from products table
        $search = $request->search;
        $date = $request->min;
        $month = $request->month;
        $query = Attendances::whereHas('user', function($query) use($search) {
            $query->whereId(auth()->user()->id);
        })
        ->with(['user.PaidLeaves' => function($query) use ($date) {
            $query->isSickToday($date)->first();
        }, 'user.roles' => function ($query) {
            $query->select('name');
        }]);


        if ($month != 0) {
            // Filter By Month
            $query->whereMonth('attendance_date', $month);
        }else{
            // Filter By Date
            if ($date != null) {
                $query->whereAttendanceDate(date('Y-m-d', strtotime($date)));
            }else{
                $query->whereAttendanceDate(date('Y-m-d'));
            }
        }
        $query->orderBy('attendance_date', 'asc');

        // $month != 0 ? $query->whereMonth('attendance_date', $month) :

        $recordsFiltered = $recordsTotal = $query->count();
        $users = $query->skip($skip)->take($pageLength)->get();

        return response()->json(["draw"=> $request->draw, "recordsTotal"=> $recordsTotal, "recordsFiltered" => $recordsFiltered, 'data' => $users], 200);
    }
}
